import React from 'react';

class Comparepwd extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            input:{},
            msg:{}
        };
        this.pwdConfirm = this.pwdConfirm.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    pwdConfirm(event){
            var inputpwd = this.state.input;
            inputpwd[event.target.name] = event.target.value;
            this.setState({
                inputpwd
            });
    }
    
    validation(){
        var msg = {};
        if (this.state.input["password"] !== this.state.input["confirmpassword"]) {
            msg["password"] = "Les mots de passe sont différents";
        }
        else {
            msg["confirmpassword"] = "Les mots de passe sont identiques";
        }
        this.setState({
            msg:msg
        });
    }

    handleSubmit(event){
        event.preventDefault();
        if (this.validation()) {
            var input = {};
            input["password"] = "";
            input["confirmpassword"] = "";
        }
    }
    render() {
        return(
            <div>
                <center>
                    <h2>Inscription</h2>
                    <hr/>
                </center>
                <form onSubmit = {this.handleSubmit}>
                    <div class = "form-group">
                        Mot de passe : 
                        <input type = "password" 
                        name = "password" value = {this.state.input.password}
                        onChange = {this.pwdConfirm}
                        class = "form-control" placeholder = "Mot de passe" required/> 
                    </div>
                    <div class = "form-group">
                        Confirmez votre mot de passe : 
                        <input type = "password" 
                        onChange = {this.pwdConfirm}
                        name = "confirmpassword" value = {this.state.input.confirmpassword}
                        class = "form-control" placeholder = "Confirmez votre mot de passe" required/>
                    </div>
                    <strong>
                    <div className = "text-danger">{this.state.msg.password}</div>
                    <div className = "text-success">{this.state.msg.confirmpassword}</div>
                    </strong>
                    <input type = "Submit" class = "button-success" value = "submit"/>
                </form>
            </div>
        );
    }
}

export default Comparepwd;