import React, { Component } from "react";
import { FlatList, View, Text } from "react-native";
import ListItem from "./ListItem";
import Separator from "./Separator";

export default ({ items, onEndReached, login }) => (
  <FlatList
  style={{paddingVertical: 10}}
    data={items}
    renderItem={({ item }) => <ListItem item={item} login={login}/>}
    keyExtractor={item => item.id}
    ItemSeparatorComponent={() => <Separator />}
    onEndReached={onEndReached}
    ListEmptyComponent={() => <Text>No songs.</Text>}
  />
);
