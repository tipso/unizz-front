import React, { Component } from "react";
import { View, Image, Text, StyleSheet, TouchableOpacity} from "react-native";
import axios from 'axios';

function sendData(login, item){
  axios({
      method: 'post',
      url: 'http://localhost:3000/users_att2/'+login,
      data: {
        musique: item
      }   
  }  
  )
}

export default ({ item: { imageUri, title, author}, login }) => (
  <TouchableOpacity style={styles.container} onClick={()=> sendData(login.login, {imageUri, title, author})}>
    <Image source={{ uri: imageUri }} style={styles.image} />
    <Text style={styles.title}>{title}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 5
  },
  image: {
    width: 100,
    height: 100,
    marginRight: 10
  },
  title: {}
});
