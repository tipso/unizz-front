import React, { useState } from 'react';
import 'react-native-gesture-handler';
import { createStackNavigator } from '@react-navigation/stack';
import { Alert, SelectPicker, StyleSheet, Text, TextInput, View, Dropdown, Picker, TouchableOpacity, Modal, Button, Platform } from 'react-native';
import axios from 'axios'
import Connexion from './connexion'
import { ScrollView } from 'react-native-gesture-handler';
import { event } from 'react-native-reanimated';
import DropDownPicker from 'react-native-dropdown-picker';
import Propositions_Profils from './Propositions_Profils';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Input } from 'react-native-elements';
import { TextInputMask } from 'react-native-masked-text'

/**
 * Component d'inscription
 */
class Inscription extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            firstname: "",
            birthdate : "",
            gender: "",
            status: "",
            pseudo: "",
            password: "",
            confirmPassword: "",
            mail: "",
            personnalities: "",
            msg: "",
            show: false,
            show1: false
        }
        //this.pwdConfirm = this.pwdConfirm.bind(this);
        //this.handleSubmit = this.handleSubmit.bind(this);
    }

    /**
     * Pour envoyer le nom de famille
     * @param event le nom de famille
     * 
     */
    handleName = (event) => {
        this.setState({
            name: event.target.value
        })
    }

    /**
     * Pour envoyer le prénom
     * @param event le prénom
     * 
     */
    handleFirstname = (event) => {
        this.setState({
            firstname: event.target.value
        })
    }
    
    /**
     * Pour envoyer la date de naissance
     * @param event la date de naissance
     * 
     */
    handleBirthdate = (event) => {
        this.setState({
            birthdate: event.target.value
        })
    }
    
    /**
     * Pour envoyer le sexe
     * @param event le sexe
     * 
     */
    handleGender = (event) => {
        this.setState({
            gender: event.target.value
        })
    }
    
    /**
     * Pour envoyer le statut professionnel
     * @param event le statut professionnel
     * 
     */
    handleStatus = (event) => {
        this.setState({
            status: event.target.value
        })
    }

    /**
     * Pour envoyer le pseudo
     * @param event le pseudo
     * 
     */
    handlePseudo = (event) => {
        this.setState({
            pseudo: event.target.value
        })
    }

    /**
     * Pour envoyer le mot de passe
     * @param event le mot de passe
     * 
     */
    handlePassword = (event) => {
        this.setState({
            password: event.target.value
        })
    }

    /**
     * Pour envoyer le mot de passe confirmé
     * @param event le mot de passe confirmé
     * 
     */
    handleConfirmPassword = async(event) => {
        //console.log(event)
        await this.setState({
            confirmPassword: event.target.value
        })
        console.log(this.state)
    }

    /**
     * Pour envoyer l'adresse email universitaire
     * @param event l'adresse email universitaire
     * 
     */
    handleMail = (event) => {
        this.setState({
            mail: event.target.value
        })
    }

    /**
     * Pour envoyer le personnage parmi les 16 Personnalities
     * @param event le personnage parmi les 16 Personnalities
     * 
     */
    handlePersonnalities = (event) => {
        this.setState({
            personnalities: event.target.value
        })
    }

    /**
     * Pour vérifier que tous les champs sont bien remplis 
     * 
     */
    validate_field = () => {
        const { name, firstname, birthdate, gender, status, pseudo, mail, personnalities, password, confirmPassword } = this.state
        if (name == "") {
            alert("Remplissez le nom")
            return false
        } else if (firstname == "") {
            alert("Remplissez le prénom")
            return false
        } else if (birthdate == "") {
            alert("Remplissez la date de naissance")
            return false
        } else if (gender == "") {
            alert("Sélectionnez le sexe")
            return false
        } else if (status == "") {
            alert("Sélectionnez le statut")
            return false
        } else if (pseudo == "") {
            alert("Remplissez le pseudo")
            return false
        } else if (mail == "") {
            alert("Remplissez l'adresse email universitaire")
            return false
        } else if (personnalities == "") {
            alert("Sélectionnez une personnalité")
            return false
        } else if (password == "") {
            alert("Remplissez le mot de passe")
            return false
        } else if (confirmPassword == "") {
            alert("Confirmez votre mot de passe")
            return false
        }
        return true
    }

    /**
     * Pour enregistrer toutes les informations du formulaire d'inscription et les envoyer dans le back
     * après avoir appuyé sur le bouton "Confirmer"
     * @param event les informations entrées lors de l'inscription
     * 
     */
    handleOnPress = (event) => {
        event.preventDefault();
        if (this.validate_field()) {
            axios({
                method: 'post',
                url: 'http://localhost:3000/register',
                data: {
                    name: this.state.name,
                    firstname: this.state.firstname,
                    birthdate: this.state.birthdate,
                    gender: this.state.gender,
                    status: this.state.status,
                    pseudo: this.state.pseudo,
                    password: this.state.password,
                    confirmPassword: this.state.confirmPassword,
                    mail: this.state.mail,
                    personnalities: this.state.personnalities,
                }
            }
            ).then((response) => {
                console.log(response);
                this.setState({ show1: true })
            })
                .catch((error) => {
                    this.setState({ show: true })
                })
        }
    }

    /**
     * Fonction qui compare "password" et "confirmPassword"
     */
    validation() {
        if (this.state.confirmPassword === this.state.password) {
            this.setState({ msg: "Validate" })
        } else {
            this.setState({ msg: "Vos mots de passe ne sont pas identiques" })
        }
    }

    /**
    * Fonction executée lorsque le champ de saisie du password est modifié
    * elle appelle la fonction "handlePassword" pour envoyer le password et la fonction "validation" pour vérifier la similitude
    * entre "password" et "confirmPassword"
    * @param event Nouveau password entré
    */
    fonctionPass = (event) => {
        this.handlePassword(event);
        this.validation();
    }

    /**
     * Fonction executée lorsque le champ de saisie du confirmPassword est modifié
     * elle appelle la foncion "handleConfirmPassword" pour envoyer le confirmpassword et la fonction "validation" pour vérifier la similitude
     * entre "password" et "confirmPassword"
     * @param event Nouveau confirmPassword entrée
     */
    fonctionConfirmationPass = async (event) => {
        await this.handleConfirmPassword(event);
        this.validation();
    }

    render() {
        return (
            <ScrollView style={styles.main_container}>
                <View style={styles.main_two}>
                    <Text> </Text>
                    <Text style={styles.textTop}>Veuillez vous enregistrer</Text>
                    <Text> </Text>
                    <TextInput
                        style={styles.textinput}
                        onChange={this.handleName}
                        placeholder="  Nom"
                    />
                    <TextInput
                        style={styles.textinput}
                        onChange={this.handleFirstname}
                        placeholder="  Prénom"
                    />
                    <TextInputMask
                        style={styles.textinput}
                        placeholder="  Date de naissance JJ/MM/AAAA"
                        type={'datetime'}
                        options={{
                            format: 'DD/MM/YYYY'
                        }}
                        value={this.state.birthdate}
                        onChangeText={text => {this.setState({ birthdate: text })
                        }}
                    />
                    <DropDownPicker style={{ alignSelf: "contain", borderWidth: 0, borderRadius: 15 }}
                        items={[
                            { label: 'Femme', value: 'Femme' },
                            { label: 'Homme', value: 'Homme' },
                            { label: 'Autres', value: 'Autres' },
                        ]}
                        disabled={false}
                        zIndex={5000}
                        defaultIndex={"0"}
                        defaultValue={this.state.gender}
                        placeholder="Sélectionnez votre sexe"
                        containerStyle={styles.picker}
                        labelStyle={styles.label}
                        dropDownStyle={{ backgroundColor: 'white' }}
                        onChangeItem={item => this.setState({
                            gender: item.value
                        })}
                    />
                    <DropDownPicker style={{ alignSelf: "contain", borderWidth: 0, borderRadius: 15 }}
                        items={[
                            { label: 'Étudiant(e)', value: 'étudiant(e)' },
                            { label: 'Professeur(e)', value: 'professeur(e)' },
                        ]}
                        zIndex={4000}
                        defaultNull
                        placeholder="Sélectionnez votre statut professionnel"
                        containerStyle={styles.picker}
                        labelStyle={styles.label}
                        dropDownStyle={{ backgroundColor: 'white' }}
                        onChangeItem={item => this.setState({
                            status: item.value
                        })}
                    />
                    <TextInput
                        style={styles.textinput}
                        onChange={this.handlePseudo}
                        placeholder="  Pseudo"
                    />
                    <TextInput
                        style={styles.textinput}
                        onChange={this.handleMail}
                        placeholder="  Adresse mail universitaire"
                    />
                    <DropDownPicker style={{ alignSelf: "contain", borderWidth: 0, borderRadius: 15 }}
                        items={[
                            { label: 'Aucun personnage', value: 'Aucun personnage' },
                            { label: 'Architecte', value: 'Architecte' },
                            { label: 'Logicien', value: 'Logicien' },
                            { label: 'Commandant', value: 'Commandant' },
                            { label: 'Innovateur', value: 'Innovateur' },
                            { label: 'Avocat', value: 'Avocat' },
                            { label: 'Médiateur', value: 'Médiateur' },
                            { label: 'Protagoniste', value: 'Protagoniste' },
                            { label: 'Inspirateur', value: 'Inspirateur' },
                            { label: 'Logisticien', value: 'Logisticien' },
                            { label: 'Défenseur', value: 'Défenseur' },
                            { label: 'Directeur', value: 'Directeur' },
                            { label: 'Consul', value: 'Consul' },
                            { label: 'Virtuose', value: 'Virtuose' },
                            { label: 'Aventurier', value: 'Aventurier' },
                            { label: 'Entrepreneur', value: 'Entrepreneur' },
                            { label: 'Amuseur', value: 'Amuseur' },
                        ]}
                        defaultIndex={0}
                        placeholder="Sélectionnez votre personnage parmi les 16Personnalities"
                        containerStyle={styles.picker}
                        labelStyle={styles.label}
                        dropDownStyle={{ backgroundColor: 'white' }}
                        onChangeItem={item => this.setState({
                            personnalities: item.value
                        })}
                    />
                    <TextInput
                        style={styles.textinput}
                        placeholder="  Mot de passe"
                        //secureTextEntry
                        onChange={this.fonctionPass}
                    />
                    <Text>{this.state.msg}</Text>
                    <TextInput
                        style={styles.textinput}
                        placeholder="  Confirmez votre mot de passe"
                        //secureTextEntry
                        onChange={(e)=>this.fonctionConfirmationPass(e)}
                    />
                    <Text>{this.state.msg}</Text>

                    <TouchableOpacity style={styles.bouton1}
                        onPress={this.handleOnPress}
                        readOnly
                    //this.setState({show:true}),
                    //this.setState({show1:true})
                    >
                        <Text>CONFIRMER</Text>
                        <Modal
                            transparent={true}
                            visible={this.state.show}
                        >
                            <View style={{ backgroundColor: "#000000aa", flex: 1 }}>
                                <View style={{ backgroundColor: "#ffffff", margin: 50, padding: 30, borderRadius: 10 }}>
                                    <Text style={styles.textModal}>Ohw tu as déjà un compte... 🤏{"\n"}Connecte-toi !</Text>
                                    <Text> </Text>
                                    <Button title="Connexion"
                                        onPress={() => {
                                            this.handleOnPress,
                                            this.setState({ show: false })
                                            this.props.navigation.navigate('Connexion')
                                        }}
                                    />
                                </View>
                            </View>
                        </Modal>
                        <Modal
                            transparent={true}
                            visible={this.state.show1}
                        >
                            <View style={{ backgroundColor: "#000000aa", flex: 1 }}>
                                <View style={{ backgroundColor: "#ffffff", margin: 50, padding: 30, borderRadius: 10 }}>
                                    <Text style={styles.textModal}>Compte créé !{"\n"}Connecte-toi !</Text>
                                    <Text> </Text>
                                    <Button title="Connexion"
                                        onPress={() => {
                                            this.setState({ show1: false })
                                            this.props.navigation.navigate('Connexion')
                                        }}
                                    />
                                </View>
                            </View>
                        </Modal>
                    </TouchableOpacity>
                    <Text> </Text>
                    <Text> </Text>
                </View>
            </ScrollView>
        )
    }
}

const Stack = createStackNavigator()
export default function MyStack() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Inscription" component={Inscription} options={{ headerShown: false }} />
            <Stack.Screen name="Connexion" component={Connexion} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    main_container: {
        marginTop: 0,
        marginBottom: 0,
        backgroundColor: '#DC4444',
        flex: 1,
    },
    main_two: {
        backgroundColor: '#DC4444',
        marginTop: 20,
        justifyContent: 'center',
    },
    textTop: {
        fontWeight: 'bold',
        fontSize: 24,
        textAlign: "center"
    },
    header: {
        fontSize: 24,
        color: '#000',
        textAlign: 'center',
        marginBottom: 40,
        borderBottomColor: '#199187',
        borderBottomWidth: 1,
    },
    textinput: {
        alignSelf: 'stretch',
        height: 50,
        marginBottom: 7,
        marginLeft: 60,
        marginRight: 60,
        marginTop: 15,
        color: '#000',
        borderWidth: 1,
        borderColor: '#000000',
        borderRadius: 10,
        alignItems: 'center',
        backgroundColor: 'white',
    },
    textModal: {
        textAlign: 'center',
    },
    picker: {
        width: "69.9%",
        height: 50,
        marginBottom: 7,
        marginLeft: 60,
        marginRight: 60,
        marginTop: 15,
        color: '#000',
        borderWidth: 1,
        borderColor: '#000000',
        borderRadius: 10,
        backgroundColor: 'white',
    },
    label: {
        alignItems: 'center',
        textAlign: 'center',
    },
    bouton1: {
        backgroundColor: '#DDDDDD',
        alignItems: 'center',
        marginTop: 15,
        marginLeft: 55,
        marginRight: 55,
        height: 35,
        justifyContent: "center",
        color: "red",
        borderRadius: 20
    }
})

