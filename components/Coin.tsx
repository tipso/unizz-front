import * as React from 'react';
import { Text, View, Button, StyleSheet, Image,SafeAreaView, TouchableOpacity, ScrollView, Alert ,FlatList} from 'react-native';
import axios from 'axios';
import { createStackNavigator } from '@react-navigation/stack';
import DemandeAide from './DemandeAide';
import PropositionAide from './PropositionAide';
import GestionUcoin from './GestionUcoin';


 

/**
 *Component permettant de naviguer vers:
 *gestion de nos U-Coins,
 *possibilité de demander de l'aide,
 *possibilité d'aider une personne ,
 * et permettant d'afficher notre solde de U-Coins
*/
class Coin extends React.Component {
    constructor(props){
        super(props)
        this.state={
            login:this.props.route.params.login,
            ucoin:""
        }
    }
    /**
     * appelle la fonction getCoin lors du chargement des U-coins
     */
    componentDidMount(){
        this.getCoin()
    }
    /**
     * Permet de récupérer le nombre de U coin d'un utilisateur
     */
    getCoin=()=>{
        axios({
            method:'GET',
            url:'http://localhost:3000/Ucoin/'+this.state.login,
            
        }).then((response) =>{
            this.setState({ucoin:response.data.ucoin})
        })
    }
    


    /**
     * Affiche l'interface d'accueil des  U coins.
     * -des  boutons pour aller vers les pages suivantes:
     *          gestion de nos U-Coins,
     *          possibilité de demander de l'aide,
     *          possibilité d'aider une personne 
     */
    render(){
        return(
            <SafeAreaView style={{backgroundColor:'#DC4444', height:"100%"}}>
                <View style={{height: "28%",marginTop:'5%',backgroundColor:'#DC4444'}}>
                        <Image style ={styles.image} source= {require ('../Images/Ucoin/ucoin.png')} />
                </View>
                <View style={styles.view_bouton}>
                <TouchableOpacity style={styles.bouton}  onPress={()=>{this.props.navigation.navigate('GestionUcoin', {login: this.state.login})}}>
                        <Text>Gérer vos U-Coins</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bouton}  onPress={()=>{this.props.navigation.navigate('DemandeAide', {login: this.state.login})}}>
                        <Text>Demander de l'aide</Text>
                    </TouchableOpacity>
                    <TouchableOpacity  style ={styles.bouton} onPress={()=>{this.props.navigation.navigate('PropositionAide', {login: this.state.login})}}>
                        <Text>Proposer de l'aide</Text>
                    </TouchableOpacity>
                </View>
                
            </SafeAreaView>
        )
    }
    
}

const Stack = createStackNavigator()
export default function MyStack({ login }) {
    
    return (
        <Stack.Navigator
        screenOptions={{
            headerShown: true,
            cardStyle: { backgroundColor: '#EEEEEE' }
          }}>
          <Stack.Screen name="Coin" component={Coin} options={{headerShown: false}} initialParams = {{login: login}}/>
          <Stack.Screen name="GestionUcoin" component={GestionUcoin}  options={{headerShown: false, title: '', headerStyle: {
            height: 40, backgroundColor: 'transparent', borderBottomWidth: 0 } }} initialParams = {{login: login}}/>
          <Stack.Screen name="DemandeAide" component={DemandeAide}  options={{headerShown: false, title: '', headerStyle: {
            height: 40, backgroundColor: 'transparent', borderBottomWidth: 0 } }} initialParams = {{login: login}}/>
          <Stack.Screen name="PropositionAide" component={PropositionAide}  options={{headerShown: false, title: '', headerStyle: {
            height: 40, backgroundColor: 'transparent', borderBottomWidth: 0 } }} initialParams = {{login: login}}/>
        </Stack.Navigator>
      );
}





const styles=StyleSheet.create({
    main:{},
    vues: {
        height:"100%",
        backgroundColor: '#EEEEEE',
        marginBottom:50,
        position:'absolute'
    },
    view_bouton:{
        height:'30%',
        justifyContent:'center'
    },
    text:{
        width: "100%",
        height: '100%',
        textAlign:"center",
        justifyContent: "center",
    },
    bouton:{
        backgroundColor: 'white',  
        marginLeft: 25,
        marginRight: 25,
        marginTop: 10,
        borderWidth: 1,
        borderColor: '#000000',
        padding: 10,
        borderRadius: 10,   
    },
    image: {
        width: "100%",
        height: "100%",
        resizeMode : 'contain',
        position: "relative",
        paddingTop:'10%',
        justifyContent: "center",
        alignItems: "center",
    }
})
