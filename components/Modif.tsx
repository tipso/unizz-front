import axios from 'axios';
import * as React from 'react';
import { Text, View, TextInput, Button, TouchableOpacity, StyleSheet, Image} from 'react-native';
import { RadioButton } from 'react-native-paper';
import SelectMultiple from 'react-native-select-multiple'
import Musique from './Musique';
import GaleriePhoto from './GaleriePhoto';
import DropDownPicker from 'react-native-dropdown-picker';
import Coin from './Coin'

const passions = [
    'Musique', 'Sport', 'Jeux vidéos', 'Musées', 'Cinéma', 'Théâtre',
    'Séries', 'Lire', 'Voyager', 'Animaux', 'Musique', 'Nature', 'Voitures',
    'Cuisine', 'Dessin', 'Humour', 'Politique', 'Informatique', 'Histoire',
    'Géographie', 'Philosophie', 'Langues étrangères', 'Sciences', 'Jeux de société'
]



/**
 * Component pour modifier son profil.
 */
class Modif extends React.Component{
    //options de navigation : afficher l'entête pour revenir à Profil.tsx sans modification
    static navigationOptions = ({navigation}) => {
        return{
            headerShown: true
        }
    }

    //Constructeur et state
    constructor(props){
        super(props)
        this.state ={
            zone: this.props.route.params.zone,
            login : this.props.route.params.login,
            picture:"",
            bio: "",
            recherche: "",
            passions: [],
            cours: "",
            musique:"",
            donnes: this.getData(),
            password:"",
            newPassword:"",
            newConfirmPassword:"",
        }
        
    }
    /**
     * Permet de modifier les passions dans le state
     * @param passions Liste des passions modifiées
     */

    /**
     * 
     */
    onSelectionsChange = (passions) => {
        this.setState({passions})
    }

    /**
     * Permet de modifier le mot de passe.
     * Envoie une requête au back qui modifie le mot de passe.
     */
    updatePassword=()=>{
        axios({
            method:'post',
            url:'http://localhost:3000/updatePassword/'+this.state.login,
            data:{
                password:this.state.password,
                newPassword:this.state.newPassword,
                newConfirmPassword:this.state.newConfirmPassword,
            }
        })
    }
    
    /**
     * Permet de récupérer les données de l'utilisateur connecté depuis le back.
     * Met à jour le state de chaque donnée.
     */
    getData(){
        axios.get(`http://localhost:3000/users_att`,{headers:{ "x-access-token": localStorage.getItem("token")}})
        .then((response) => {
         this.setState({
            picture: response.data.[this.state.login].picture,
            bio: response.data.[this.state.login].bio,
            recherche: response.data.[this.state.login].recherche,
            passions: response.data.[this.state.login].passions,
            cours: response.data.[this.state.login].cours,
            musique: response.data.[this.state.login].musique
         })

       })
      .catch((error)=>{
         console.log(error);
      });
    }

    /**
     * Envoie le state comprenant les données de l'utilisateur modifiées au back.
     */
    sendData(){
        axios({
            method: 'post',
            url: 'http://localhost:3000/users_att/'+this.state.login,
            data: {
              picture: this.state.picture,
              bio: this.state.bio,
              recherche: this.state.recherche,
              passions: this.state.passions,
              cours: this.state.cours,
              musique: this.state.musique,
              selectedPassions: this.state.selectedPassions
            },
            headers:{ "x-access-token": localStorage.getItem("token")},   
        }  
        )
    }

    /**
     * Modifier la biographie
     * @param event Nouvelle biographie entrée
     */
    handleBio = (event) => {
        this.setState({
            bio: event.target.value,
        })
    }
    /**
     * Modifier l'ancien mot de passe
     * @param event Ancien mot de passe entré
     */
    handlePassword =(event) => {
        this.setState({
            password: event.target.value
        })
    }
    /**
     * Récupérer le nouveau mot de passe
     * @param event Nouveau mot de passe
     */
    handleNewPassword =(event) => {
        this.setState({
            newPassword: event.target.value
        })
    }
    /**
     * Vérifier le nouveau mot de passe 
     * @param event Nouveau mot de passe
     */
    handleNewConfirmPassword =(event) => {
        this.setState({
            newConfirmPassword: event.target.value
        })
    }
    
    
    

    render(){
        
        //Pour afficher la modification de la biographie
        if (this.state.zone=="bio"){
            return(
                <View style={styles.vues}>
                    <View style={{height: "28%"}}>
                        <Image style ={styles.image} source= {require ('../Images/Modif/m_bio.png')} />
                    </View>
                    <Text style={{fontWeight: "bold", textAlign: "center", fontSize: 17}}>Vous pouvez modifier votre description</Text>
                    <TextInput style={styles.textinput} multiline numberOfLines={15}
                    placeholder = "Biographie" onChange = {this.handleBio} value = {this.state.bio}/>
    
                    <View >
                        <TouchableOpacity style={styles.boutton} onPress={() => {
                        this.sendData();
                        this.props.navigation.goBack();
                        }}> <Text style={{fontWeight: "bold", color:"white"}}>Valider</Text></TouchableOpacity>
                    </View>
                </View>
            )
        }
        //Pour afficher la modification de la recherche
        if (this.state.zone=="recherche"){
            return(
                <View style={styles.vues}>
                    <View style={{height: "28%"}}>
                        <Image style ={styles.image} source= {require ('../Images/Modif/m_recherche.png')} />
                    </View>
                    <Text style={{fontWeight: "bold", textAlign: "center", fontSize: 17, marginBottom: 15}}>Vous pouvez modifier ce que vous recherchez</Text>
                    <View style={styles.textinput}>
                    <View style={{flexDirection:"row",alignItems:'center'}}>
                    <View style={{flex:4}}>
                    <Text style={{fontSize: 15}}>Recherhe amour</Text>
                    </View>
                    <View style={{flex:1}}>
                    <RadioButton
                        value="value1"
                        status={ this.state.recherche == 'value1' ? 'checked' : 'unchecked' }
                        onPress={() => this.setState({recherche: 'value1'})}
                    />
                    </View>
                    </View>

                    <View style={{flexDirection:"row",alignItems:'center'}}>
                    <View style={{flex:4}}>
                    <Text style={{fontSize: 15}}>Recherche amitié</Text>
                    </View>
                    <View style={{flex:1}}>
                    <RadioButton
                        value="value2"
                        status={ this.state.recherche == 'value2' ? 'checked' : 'unchecked' }
                        onPress={() => this.setState({recherche: 'value2'})}
                    />
                    </View>
                    </View>

                    <View style={{flexDirection:"row",alignItems:'center'}}>
                    <View style={{flex:4}}>
                    <Text style={{fontSize: 15}}>Les deux</Text>
                    </View>
                    <View style={{flex:1}}>
                    <RadioButton
                        value="value3"
                        status={ this.state.recherche == 'value3' ? 'checked' : 'unchecked' }
                        onPress={() => this.setState({recherche: 'value3'})}
                    />
                    </View>
                    </View>
                    </View>
                    
                    
                    <View >
                        <TouchableOpacity style={styles.boutton} onPress={() => {
                        this.sendData();
                        this.props.navigation.goBack();
                        }}> <Text style={{fontWeight: "bold", color:"white"}}>Valider</Text></TouchableOpacity>
                    </View>
                </View>
            )
        }

        //Pour afficher la modification des passions
        if (this.state.zone=="passions"){
            return(
                <View style={styles.vues}>
                    <View style={{height: "28%"}}>
                        <Image style ={styles.image} source= {require ('../Images/Modif/m_passion.png')} />
                    </View>
                    <Text style={{fontWeight: "bold", textAlign: "center", fontSize: 17}}>Vous pouvez modifier vos passions</Text>
                    <SelectMultiple checkboxSource={require('../Images/Modif/icon-checkbox.png')} selectedCheckboxSource={require('../Images/Modif/icon-checkbox.png')} style= {styles.textinput}
                    items={passions}
                    selectedItems={this.state.passions}
                    onSelectionsChange={this.onSelectionsChange} />

                    <View >
                        <TouchableOpacity style={styles.boutton} onPress={() => {
                        this.sendData();
                        this.props.navigation.goBack();
                        }}> <Text style={{fontWeight: "bold", color:"white"}}>Valider</Text></TouchableOpacity>
                    </View>
                </View>
            )
        }
        //Pour afficher la modification des cours
        if (this.state.zone=="cours"){
            return(
                <View style={styles.vues}>
                    <View style={{height: "28%"}}>
                        <Image style ={styles.image} source= {require ('../Images/Modif/m_cours.png')} />
                    </View>
                    <Text style={{fontWeight: "bold", textAlign: "center", fontSize: 17}}>Vous pouvez modifier votre cursus</Text>
                    <DropDownPicker 
                items={[
                    {label: 'Sciences sociales', value: 'Sciences sociales', untouchable: true, textStyle:{fontWeight: "bold"} },
                    {label: 'L1 Sciences sociales', value: 'L1 Sciences sociales', parent: 'Sciences sociales'},
                    {label: 'L2 Sciences sociales', value: 'L2 Sciences sociales', parent: 'Sciences sociales'},
                    {label: 'L3 Sciences sociales', value: 'L3 Sciences sociales', parent: 'Sciences sociales'},

                    {label: 'Sciences de la vie et de la Terre', value: 'Sciences de la vie et de la Terre', untouchable: true, textStyle:{fontWeight: "bold"} }, 
                    {label: 'L1 Sciences de la vie et de la Terre', value: 'L1 Sciences de la vie et de la Terre', parent: 'Sciences de la vie et de la Terre'},
                    {label: 'L2 Sciences de la vie et de la Terre', value: 'L2 Sciences de la vie et de la Terre', parent: 'Sciences de la vie et de la Terre'},
                    {label: 'L3 Sciences de la vie et de la Terre', value: 'L3 Sciences de la vie et de la Terre', parent: 'Sciences de la vie et de la Terre'},

                    {label: 'Economie / Gestion', value: 'eco/ges', untouchable: true, textStyle:{fontWeight: "bold"} }, 
                    {label: 'L1 Economie / Gestion', value: 'L1 Economie / Gestion', parent: 'eco/ges'},
                    {label: 'L2 Economie / Gestion', value: 'L2 Economie / Gestion', parent: 'eco/ges'},
                    {label: 'L3 Economie / Gestion', value: 'L3 Economie / Gestion', parent: 'eco/ges'},

                    {label: 'STAPS', value: 'STAPS', untouchable: true, textStyle:{fontWeight: "bold"} }, 
                    {label: 'L1 STAPS', value: 'L1 STAPS', parent: 'STAPS'},
                    {label: 'L2 STAPS', value: 'L2 STAPS', parent: 'STAPS'},
                    {label: 'L3 STAPS', value: 'L3 STAPS', parent: 'STAPS'},

                    {label: 'Sciences pour la santé', value: 'ssante', untouchable: true, textStyle:{fontWeight: "bold"} },
                    {label: 'L1 Sciences pour la santé', value: 'L1 Sciences pour la santé', parent: 'ssante'},
                    {label: 'L2 Sciences pour la santé', value: 'L2 Sciences pour la santé', parent: 'ssante'},
                    {label: 'L3 Sciences pour la santé', value: 'L3 Sciences pour la santé', parent: 'ssante'},

                    {label: 'Mathématiques', value: 'maths', untouchable: true, textStyle:{fontWeight: "bold"} },
                    {label: 'L1 Mathématiques', value: 'L1 Mathématiques', parent: 'maths'},
                    {label: 'L2 Mathématiques', value: 'L2 Mathématiques', parent: 'maths'},
                    {label: 'L3 Mathématiques', value: 'L3 Mathématiques', parent: 'maths'},

                    {label: 'Informatique', value: 'info', untouchable: true, textStyle:{fontWeight: "bold"} },
                    {label: 'L1 Informatique', value: 'L1 Informatique', parent: 'info'},
                    {label: 'L2 Informatique', value: 'L2 Informatique', parent: 'info'},
                    {label: 'L3 Informatique', value: 'L3 Informatique', parent: 'info'},
                ]}
                defaultValue={this.state.cours}
                style={styles.textinput}
                itemStyle={{justifyContent: 'flex-start'}}
                dropDownStyle={{backgroundColor: 'white',  
                borderWidth: 1,
                borderColor: '#000000',
                borderRadius: 10,
                marginTop: 12,
                marginRight: 25,
                marginLeft: 25,
                width: "88%",
                alignSelf: "center"}}
                onChangeItem={item => this.setState({ cours: item.value})}
                placeholder="Selectionnez un cours"
                labelStyle={{
                    fontSize: 14,
                    textAlign: 'left',
                }}
                />
    
                    <View >
                        <TouchableOpacity style={styles.boutton} onPress={() => {
                        this.sendData();
                        this.props.navigation.goBack();
                        }}> <Text style={{fontWeight: "bold", color:"white"}}>Valider</Text></TouchableOpacity>
                    </View>
                </View>
            )
        }
        //Pour afficher la modification de la musique
        if (this.state.zone=="musique"){
            return(
                <View style={styles.vues}>
                    <View style={{height: "28%"}}>
                        <Image style ={styles.image} source= {require ('../Images/Modif/m_musique.png')} />
                    </View>
                    <Text style={{fontWeight: "bold", textAlign: "center", fontSize: 17, marginBottom: 15}}>Vous pouvez modifier votre musique favorite</Text>
                    <Musique login={this.state.login}/>
                </View>
            )
        }
        //Pour gerer l'ajout de photos
        if (this.state.zone=="picture"){
            console.log(this.state)
            return(
                <View style={styles.vues}>
                    <View style={{height: "28%"}}>
                        <Image style ={styles.image} source= {require ('../Images/Modif/m_profil.png')} />
                    </View>
                    <Text style={{fontWeight: "bold", textAlign: "center", fontSize: 17, marginBottom: 15}}>Vous pouvez modifier vos photos</Text>

                    <GaleriePhoto login={this.state.login} musique={this.state.musique}/> 
                </View>
                
            )
        } 
        if(this.state.zone=="parametres"){
            console.log(this.state)
            return(
                <View style={styles.vues}>
                    <View style={{height: "28%",marginTop:'5%'}}>
                        <Image style ={styles.image} source= {require ('../Images/Modif/parametres.png')} />
                    </View>
                    <Text style={{fontWeight: "bold", textAlign: "center", fontSize: 17}}>Vous pouvez modifier votre mot de passe</Text>
                    <View style={{height:'30%', justifyContent:'center'}}>
                        <TextInput style={styles.textinput} secureTextEntry={true} placeholder="Veuillez saissir votre mot de passe." onChange={this.handlePassword}></TextInput>
                        <TextInput style={styles.textinput} secureTextEntry={true} placeholder="Veuillez saissir votre nouveau mot de passe." onChange={this.handleNewPassword}></TextInput>
                        <TextInput style={styles.textinput} secureTextEntry={true} placeholder="Veuillez confirmer votre mot de passe." onChange={this.handleNewConfirmPassword}></TextInput>
                    </View>
                    
                    <View style={{marginBottom:'10%'}} >
                        <TouchableOpacity style={styles.boutton} onPress={()=>{this.updatePassword();this.props.navigation.goBack();}}>
                            <Text style={{fontWeight: "bold", color:"white"}}>Valider</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }

        if (this.state.zone=="Coin"){
            console.log(this.state.login)
            return(
                <View style={styles.vues}>
                    <Coin login={this.state.login}/>
                </View>
                
            )
        }
    }
}


const styles = StyleSheet.create({
    boutton: {
        backgroundColor: '#DC4444',
        alignItems: "center",
        marginTop: 20,  
        marginLeft: 55,
        marginRight: 55,
        height: 35,
        justifyContent: "center",
        color: "red",
        borderRadius: 20,
      
    },
    vues: {
        height:"100%",
        backgroundColor: '#EEEEEE',
        paddingBottom: 10
    },

    textinput : {
      
        backgroundColor: 'white',  
        marginLeft: 25,
        marginRight: 25,
        marginTop: 10,
        borderWidth: 1,
        borderColor: '#000000',
        padding: 10,
        borderRadius: 10,   
      },

      image: {
        width: "100%",
        height: "100%",
        resizeMode : 'contain',
        alignSelf:"center",
        position: "absolute",
        bottom: "20%"
      }

})

export default Modif