import React, { Component } from 'react';
import { View, Text, SafeAreaView, StyleSheet, FlatList, Button, Image,Alert } from 'react-native';
import axios from 'axios';
import camera  from 'expo-camera';
import * as Permissions from 'expo-permissions'

import * as ImagePicker from 'expo-image-picker';
import { TouchableOpacity } from 'react-native-gesture-handler';

const urlImage = 'https://pokeres.bastionbot.org/images/pokemon/';

/**
 * Component permettant de mofidier les photos de l'utilisateur.
 */
class GaleriePhoto extends Component {
    
    
    constructor(login) {
        super(login);
        this.state = {
          login: login.login,
          data: [],
          uri:"",
        
        
        };
    }

    /**
     * Supprime toutes les photos d'un utilisateur
     */
    reset(){
      axios({
        method: 'post',
        url: 'http://localhost:3000/zero_photo/'+this.state.login,
        data: {
          zero: []
        }   
      } )
      .then(()=> this.setState({uri: ""}))
    }
    /**
     * Demande à l'utilisateur l'accès à la caméra 
     */
    askForPermission = async () => {
      const permissionResult = await Permissions.askAsync(Permissions.CAMERA)
      if (permissionResult.status !== 'granted') {
        Alert.alert('no permissions to access camera!', [{ text: 'ok' }])
        return false
      }
      return true
    }
  /**
   * Permet à l'utilisateur de prendre une photo avec sa caméra.
   */
    takeImage = async () => {
      // make sure that we have the permission
      const hasPermission = await this.askForPermission()
      if (!hasPermission) {
        return
      } else {
        // launch the camera with the following settings
        let image = await ImagePicker.launchCameraAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: true,
          aspect: [3, 3],
          quality: 1,
          base64: false,
        })
        
        if (!image.cancelled) {
          //setImage(result.uri);
          axios({
              method:'POST',
              url:'http://localhost:3000/images1/'+this.state.login,
              data:{
                imgsource: image
              }
          }).then((response) =>{
            this.setState({uri:response.data});
          })
      }
      }
    }
/**
   * Permet à l'utilisateur de sélectionner une photo dans sa galerie.
   */
    pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        quality: 1,
        base64:false
        }); 
        if (!result.cancelled) {
            //setImage(result.uri);
            axios({
                method:'POST',
                url:'http://localhost:3000/images2/'+this.state.login,
				        data:{
                  imgsource: result
                }
            }).then((response) =>{
              this.setState({uri:response.data});
            })
        }
    };
/**
 * Récupère les photos de l'utilisateur dans le back
 */
    getImage= async () => {
      try {
        const res = await  axios.get('http://localhost:3000/images1/'+this.state.login)
        if (res) {
          this.setState({uri:res.data})
        }
      } catch (error) {
        console.log('error:', error);
      }
    }

  componentDidMount() {
    this.getImage();
    
  }

  render() {
    const { data } = this.state;
    return (
      <SafeAreaView style={{flex: 1}}>
        <FlatList
          numColumns={2}
          data = {this.state.uri}
          renderItem={({item}) => (
            <Image
              style={styles.item}
              source={item}
            />
          )}
            style={styles.container}
          />
        
            
            <TouchableOpacity style={styles.bouton} onPress={() => {this.pickImage()}}><Text style={{fontWeight: "bold", color:"white",textAlign:"center"}}>Importer des photos depuis la galerie</Text> </TouchableOpacity >
            <TouchableOpacity style={styles.bouton} onPress={() => {this.takeImage()}}><Text style={{fontWeight: "bold", color:"white"}}>Activer la caméra</Text> </TouchableOpacity >
            <TouchableOpacity style={styles.bouton} onPress={() => {this.reset()}}><Text style={{fontWeight: "bold", color:"white"}}>Supprimer les photos</Text> </TouchableOpacity >
            
        
      </SafeAreaView>
    );
  }
}

export default GaleriePhoto

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "black",
    borderRadius:10,
    marginHorizontal: 25,
  },
  item: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginTop: 8,
    marginHorizontal: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    
    elevation: 5,
    height:100,
    width: 100,
    resizeMode:'contain'
  },
  image: {
    height:100,
    width: 100,
    resizeMode:'stretch'
  },
  text: {
    color: 'orange',
    fontWeight:'bold'
  },
  bouton:{
    backgroundColor: '#DC4444',
    alignItems: "center",
    marginTop: 10,  
    marginLeft: 55,
    marginRight: 55,
    height: 35,
    justifyContent: "center",
    color: "red",
    borderRadius: 20,
  }
})