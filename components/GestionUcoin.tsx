import * as React from 'react';
import { Text, View,TextInput, Button,SafeAreaView,Modal, StyleSheet, Image, TouchableOpacity, ScrollView, Alert ,FlatList} from 'react-native';
import axios from 'axios';
import DropDownPicker from 'react-native-dropdown-picker';
import { Searchbar } from "react-native-paper";
import Separator from '../src_musique/components/Separator';
import styles1 from '../Components/ChatListItems/Styles';

/**
 * Classe permettant à un utilisateur de réaliser un virement vers un ami, de voir son historique 
 */
class GestionUcoin extends React.Component {
    constructor(props){
        super(props);
        this.state={
            login : this.props.route.params.login,
            ucoin:"",
            modaltransaction:false,
            personneselected:"",
            data:[],
            coinToGive:"",
            historique:[],
            historiqueLoaded:false
        }
    }

    componentDidMount(){
        this.returnUcoin()
        this.nameFirstname()
    }
    /**
     * Permet de récuperer le nom, le prénom et la photo de profil de tous les matchs de l'utilisateur connecté
     */
    nameFirstname=()=>{
        axios({
           method:'GET',
           url:'http://localhost:3000/transaction/'+this.state.login,
        }).then(response=>{
          this.setState({
            data:response.data
          })
        })
      }
    
    /**
     * Permet de recupérer les U-coins de la personne connectée
     */
    returnUcoin=()=>{
        axios({
            method:'GET',
            url:'http://localhost:3000/Ucoin/'+this.state.login,
            
        }).then((response) =>{
            this.setState({ucoin:response.data.ucoin})
        })
    }
    giveUcoin=()=>{
        axios({
            method:'POST',
            url:'http://localhost:3000/giveUcoin/'+this.state.login,
            data:{
                coins:this.state.coinToGive,
                to: this.state.personneselected.item.email
            }
        }).then(() =>{
            this.setState({
                personneselected:"",
                modaltransaction:false
            })
        })
    }
    historique=()=>{
        axios({
            method:'GET',
            url:'http://localhost:3000/historique/'+this.state.login,
        }).then((response)=>{
            this.setState({historique:response.data,historiqueLoaded:true})
        })
    }

    handleCoin= (event) => {
        this.setState({
            coinToGive: event.target.value
        })
    }


    

    render (){

        if(this.state.historiqueLoaded){
            console.log(this.state)
            return(
                <View style={{backgroundColor:"#000000aa",justifyContent: 'center',height:'100%'}}>
                    <FlatList 
                        style={{width: '100%',height:'100%',borderRadius:10}}
                        data={this.state.historique}
                        renderItem={({item})=> (
                        <TouchableOpacity style={styles.textinput}>
                            <Image source={item.pdp[0]} style={styles1.avatar}/>
                            <Text style={styles.nom}>{item.to}</Text>
                            <Text style={styles.montant}>{item.montant}</Text>
                        </TouchableOpacity>
                        )}
                        keyExtractor={(item) => item}
                    />
                </View>
            )
        }
        if(this.state.personneselected!==""){
            console.log(this.state)
            return(
                <View style={{backgroundColor:"#000000aa",justifyContent: 'center',height:'100%'}}>
                    <View style={{height:'30%'}} >
                        <Image source={this.state.personneselected.item.pdp[0]} style={styles.photocenter}/>
                    </View>
                    
                    <TextInput
                        style={styles.textinputcoin}
                        keyboardType='numeric'
                        onChange={this.handleCoin}
                        placeholder="montant"
                        value={this.state.coinToGive}
                    />

                    <TouchableOpacity style={styles.boutton} onPress={()=>this.giveUcoin()}>
                        <Text style={styles.text}>Donner</Text>
                    </TouchableOpacity>

                </View>
            )
        }

        return(
            
            <SafeAreaView style={{backgroundColor:'#DC4444', height:"100%"}}>
                <View style={styles.circle}>
                    <Text style={styles.ucoins}>solde: {this.state.ucoin} U-Coins</Text>
                </View>
                    
                
                <TouchableOpacity style={styles.textinput} onPress={()=>this.setState({modaltransaction:true})}>
                    <Text style={styles.text}> Donner des U-COINS</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.textinput} onPress={()=>this.historique()}>
                    <Text style={styles.text}> Historique des donnations</Text>
                </TouchableOpacity>
                
                    <Modal 
                            transparent={false}
                            visible={this.state.modaltransaction}>
                         <View style={{backgroundColor:"#000000aa", flex:1}}>                  
                            <FlatList 
                                style={{width: '100%',height:'100%',borderRadius:10}}
                                data={this.state.data}
                                renderItem={({item})=> (
                                    <TouchableOpacity style={styles.textinput} onPress={()=>{this.setState({personneselected:{item}})}}>
                                        <Image source={item.pdp[0]} style={styles1.avatar}/>
                                        <Text style={styles.nom}>{item.name}</Text>
                                    </TouchableOpacity>
                                )}
                                keyExtractor={(item) => item.email}
                            />
                             <TouchableOpacity style={styles.boutton}
                                onPress={() => {this.setState({modaltransaction:false})}}>
                                <Text> Revenir aux propositions</Text>
                            </TouchableOpacity>
                        </View> 
                    </Modal>
            </SafeAreaView>
        )
    }

}

const styles = StyleSheet.create({
    
    main:{
        height:'100%',
        width:'100%'
    },

    nom:{
        width: "100%",
        height: 80,
        textAlign:"center",
        justifyContent: "center",
        fontWeight: "bold",
        borderColor: "green",
        position: "absolute",
        borborderWidth : 3,
        marginBottom: '10%',
        
    },
    photo:{
        width: 70,
        height: 70,
        margin: 5,
        resizeMode: 'strech',
        borderRadius:35,

    },
    photocenter:{
        width: 140,
        height: 140,
        resizeMode: 'strech',
        borderRadius:70,
        alignSelf:'center',
        alignItems:'center',
        justifyContent: 'center'

    },
    montant:{
        
        textAlign:'center',
        paddingLeft: '15%'
    },
    ucoins:{
        textAlign:'center',
        color: '#E8F4FA'
    },
    circle :{height: 100,
         width: 100,  
         borderRadius: 50, 
         resizeMode : 'strech',
         backgroundColor: '#007CBF',
         marginTop:'3%', 
         justifyContent: 'center',
         alignSelf:'center'

         
    },
    text:{
        textAlign:'center',
        color: '#0F4966'
    },

    barre: {
        marginHorizontal: 20,
        borderWidth: 1,
        borderColor: '#000000',
        borderRadius:10,
        marginTop: 10,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
    },

    textinput1 : {
        height: "4%",
        backgroundColor: 'white',  
        marginHorizontal: 20,
        marginTop: '20%',
        borderWidth: 1,
        borderColor: '#000000',
        paddingHorizontal: 5,
        borderRadius: 20,
    },
    textinput : {
        backgroundColor: 'white',  
        marginLeft: 25,
        marginRight: 25,
        marginTop: 10,
        borderWidth: 1,
        borderColor: '#000000',
        padding: 10,
        borderRadius: 10,
    },
    textinputcoin : {
        backgroundColor: 'white',  
        marginLeft: 25,
        marginRight: 25,
        borderWidth: 1,
        borderColor: '#000000',
        padding: 10,
        borderRadius: 10,
        textAlign:'center'
    },
    boutton: {
        backgroundColor: '#DC4444',
        alignItems: "center",
        marginTop: 20,  
        marginLeft: 55,
        marginRight: 55,
        height: 35,
        justifyContent: "center",
        color: "red",
        borderRadius: 20,
      
    },
    name:{
        alignSelf:'center',
        
        fontWeight: "bold",
    }
})
export default GestionUcoin;
