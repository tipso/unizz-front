import React, { Component } from 'react';
import 'react-native-gesture-handler';
import axios from 'axios' ;
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image} from 'react-native';

/**
 * Component permettant la recuperation de son mot de passe 
 */
class FogotPassword extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            mail: ""
        }
    }
 /**
  * Fonction qui met à jour le mail
  * @param event le mail entré par l'utilisateur
  */
    changeHandler = (event) => {
        this.setState({
            mail: event.target.value})
    }
    /*
    * Envoie le mail au back
    */
    submitHandler = () => axios({
        method : 'post' ,
        url: 'http://localhost:3000/recover_password',
        data : {
            email : this.state.mail
        }
    }
    ).catch(error => {
        console.log(error)
    })
        
    render(){
        return(
            <View style = {styles.main_container} >
                <View style={{flex: 0.75, marginTop: 10}}>
                    <Image style ={styles.image_unizz} source= {require ('./Images/un.png')} />
                </View>
                <View style={{flex: 0.5, marginHorizontal: 22}}>
                    <Text style={{textAlign:"Center", fontSize:20,  fontWeight:"bold", margin:10, marginBottom: 40, textDecorationLine: "underline"}}>Réinitialiser le mot de passe</Text>
                    <Text style={{textAlign:"Center", fontSize:17, color:"#DDDDDD", margin:10, marginHorizontal: 20}}>Si vous avez oublié votre mot de passe actuel, vous pouvez le modifier.</Text>                    
                </View> 
                <View style = {{flex:2, marginBottom: 15}}> 
                    <TextInput autoCompleteType= "email" style = {[styles.textInput]}  placeholder = "Adresse e-mail" onChange = {this.changeHandler} ></TextInput>
                    <TouchableOpacity style={styles.bouton} onPress={this.submitHandler} >
                        <Text style={{fontWeight: "bold"}}>Envoyer</Text>
                    </TouchableOpacity>
                </View>
                <View style = {{backgroundColor:'white', flex: 0.5, justifyContent: 'center'}}>
                <Text style={{textAlign:"center", fontStyle: "italic", paddingHorizontal:5}}>Ne communiquez jamais votre mot de passe, même à un membre de l'équipe Unizz.</Text>
                </View>    
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    main_container : {
        backgroundColor: '#DC4444',
        flex:1,
    },

    image_unizz : {
        alignSelf: "center",
        height: 200,
        width: 200,
        resizeMode: "contain",
        position: "absolute",
        bottom: "0%",
    },

    textInput : {
      backgroundColor: 'white',  
      marginLeft: 35,
      marginRight: 35,
      marginTop: 80,
      marginbottom: 30,
      height: 50,
      padding: 10,
      borderWidth: 1,
      borderColor: '#000000',
      borderRadius: 10
      
    },

    bouton: {
        backgroundColor: '#DDDDDD',
        alignItems: "center",
        marginTop: 20,  
        marginLeft: 55,
        marginRight: 55,
        height: 35,
        justifyContent: "center",
        color: "red",
        borderRadius: 20,
    },
  })

  export default FogotPassword