import * as React from 'react';
import { Text, View,TextInput, Button,SafeAreaView, StyleSheet, Image, TouchableOpacity, ScrollView, Alert ,FlatList} from 'react-native';
import axios from 'axios';
import DropDownPicker from 'react-native-dropdown-picker';
import { Searchbar } from "react-native-paper";
import Separator from '../src_musique/components/Separator';


 /**
 * Component permettant à l'utilisateur de demander de l'aide sur un sujet 
 */
class DemandeAide extends React.Component {
    constructor(props){
        super(props);
        this.state={
            login : this.props.route.params.login,
            cours:"",
            sujetDeLaDemande:"",
            descriptionDetaillee:"",
            ucoins:""
        }
    }
    /**
     * Fonction permettant  de mettre à jour le state
     * prend en paramètre @event, qui correspond au texte entré dans la selection du sujet
     */
    handleSujet=(event)=>{
        this.setState({
            sujetDeLaDemande:event.target.value
        })
    }
    /**
     * Fonction permettant  de mettre à jour le state
     * prend en paramètre @event, qui correspond au texte entré dans le champs détaillé
     */
    handleDetaillee=(event)=>{
        this.setState({
            descriptionDetaillee:event.target.value
        })
    }
    
    /**
     * Permet d'envoyer la demande vers le serveur une fois que l'utlisateur clique sur 'Envoyer'
     */
    sendAskHelp=()=>{
        console.log(this.props)
        axios({
            method:'POST',
            url:'http://localhost:3000/demandeCours/'+this.state.login,
            data:{
                cours:this.state.cours,
                topic:this.state.sujetDeLaDemande,
                demande:this.state.descriptionDetaillee,
                ucoins:this.state.ucoins
            }
        }).then((response) =>{
            this.setState({data:response.data});
        })
    } 
    /**
     * Fonction permettant de remettre tous les champs à 0 lorsqu'un utilisateur clique sur 'Annuler'
     */
    reset=()=>{
        this.setState({
            cours:"",
            sujetDeLaDemande:"",
            descriptionDetaillee:"",    
            UCOINS:""   
        })
    }

    
    render (){
        return(

            <SafeAreaView style={{backgroundColor:'#DC4444', height:"100%"}}>
                
                
                    <DropDownPicker 
                    items={[
                        {label: 'Sciences sociales', value: 'Sciences sociales', untouchable: true, textStyle:{fontWeight: "bold"} },
                        {label: 'L1 Sciences sociales', value: 'L1 Sciences sociales', parent: 'Sciences sociales'},
                        {label: 'L2 Sciences sociales', value: 'L2 Sciences sociales', parent: 'Sciences sociales'},
                        {label: 'L3 Sciences sociales', value: 'L3 Sciences sociales', parent: 'Sciences sociales'},
                        {label: 'M1 Sciences sociales', value: 'M1 Sciences sociales', parent: 'Sciences sociales'},
                        {label: 'M2 Sciences sociales', value: 'M2 Sciences sociales', parent: 'Sciences sociales'},

                        {label: 'Sciences de la vie et de la Terre', value: 'Sciences de la vie et de la Terre', untouchable: true, textStyle:{fontWeight: "bold"} }, 
                        {label: 'L1 Sciences de la vie et de la Terre', value: 'L1 Sciences de la vie et de la Terre', parent: 'Sciences de la vie et de la Terre'},
                        {label: 'L2 Sciences de la vie et de la Terre', value: 'L2 Sciences de la vie et de la Terre', parent: 'Sciences de la vie et de la Terre'},
                        {label: 'L3 Sciences de la vie et de la Terre', value: 'L3 Sciences de la vie et de la Terre', parent: 'Sciences de la vie et de la Terre'},
                        {label: 'M1 Sciences de la vie et de la Terre', value: 'M1 Sciences de la vie et de la Terre', parent: 'Sciences de la vie et de la Terre'},
                        {label: 'M2 Sciences de la vie et de la Terre', value: 'M2 Sciences de la vie et de la Terre', parent: 'Sciences de la vie et de la Terre'},

                        {label: 'Economie / Gestion', value: 'eco/ges', untouchable: true, textStyle:{fontWeight: "bold"} }, 
                        {label: 'L1 Economie / Gestion', value: 'L1 Economie / Gestion', parent: 'eco/ges'},
                        {label: 'L2 Economie / Gestion', value: 'L2 Economie / Gestion', parent: 'eco/ges'},
                        {label: 'L3 Economie / Gestion', value: 'L3 Economie / Gestion', parent: 'eco/ges'},
                        {label: 'M1 Economie / Gestion', value: 'M1 Economie / Gestion', parent: 'eco/ges'},
                        {label: 'M2 Economie / Gestion', value: 'M2 Economie / Gestion', parent: 'eco/ges'},

                        {label: 'STAPS', value: 'STAPS', untouchable: true, textStyle:{fontWeight: "bold"} }, 
                        {label: 'L1 STAPS', value: 'L1 STAPS', parent: 'STAPS'},
                        {label: 'L2 STAPS', value: 'L2 STAPS', parent: 'STAPS'},
                        {label: 'L3 STAPS', value: 'L3 STAPS', parent: 'STAPS'},
                        {label: 'M1 STAPS', value: 'M1 STAPS', parent: 'STAPS'},
                        {label: 'M2 STAPS', value: 'M2 STAPS', parent: 'STAPS'},

                        {label: 'Sciences pour la santé', value: 'ssante', untouchable: true, textStyle:{fontWeight: "bold"} },
                        {label: 'L1 Sciences pour la santé', value: 'L1 Sciences pour la santé', parent: 'ssante'},
                        {label: 'L2 Sciences pour la santé', value: 'L2 Sciences pour la santé', parent: 'ssante'},
                        {label: 'L3 Sciences pour la santé', value: 'L3 Sciences pour la santé', parent: 'ssante'},
                        {label: 'M1 Sciences pour la santé', value: 'M1 Sciences pour la santé', parent: 'ssante'},
                        {label: 'M2 Sciences pour la santé', value: 'M2 Sciences pour la santé', parent: 'ssante'},

                        {label: 'Mathématiques', value: 'maths', untouchable: true, textStyle:{fontWeight: "bold"} },
                        {label: 'L1 Mathématiques', value: 'L1 Mathématiques', parent: 'maths'},
                        {label: 'L2 Mathématiques', value: 'L2 Mathématiques', parent: 'maths'},
                        {label: 'L3 Mathématiques', value: 'L3 Mathématiques', parent: 'maths'},
                        {label: 'M1 Mathématiques', value: 'M1 Mathématiques', parent: 'maths'},
                        {label: 'M2 Mathématiques', value: 'M2 Mathématiques', parent: 'maths'},

                        {label: 'Informatique', value: 'info', untouchable: true, textStyle:{fontWeight: "bold"} },
                        {label: 'L1 Informatique', value: 'L1 Informatique', parent: 'info'},
                        {label: 'L2 Informatique', value: 'L2 Informatique', parent: 'info'},
                        {label: 'L3 Informatique', value: 'L3 Informatique', parent: 'info'},
                        {label: 'M1 Informatique', value: 'M1 Informatique', parent: 'info'},
                        {label: 'M2 Informatique', value: 'M2 Informatique', parent: 'info'},
                    ]}
                    
                    style={styles.textinput}
                    itemStyle={{justifyContent: 'flex-start'}}
                    dropDownStyle={{backgroundColor: 'white',  
                    borderWidth: 1,
                    borderColor: '#000000',
                    borderRadius: 10,
                    marginTop: 12,
                    marginRight: 25,
                    marginLeft: 25,
                    width: "88%",
                    alignSelf: "center"}}
                    onChangeItem={item => this.setState({ cours: item.value})}
                    placeholder="Selectionnez un cours"
                    labelStyle={{
                        fontSize: 14,
                        textAlign: 'left',
                    }}
                    />
                
                    <TextInput style={styles.textinput} multiline numberOfLines={2}
                        placeholder = "Sujet" onChange = {this.handleSujet} value = {this.state.sujetDeLaDemande}
                    />
                    <TextInput style={styles.textinput} multiline numberOfLines={20}
                        placeholder = "Description détaillée " onChange = {this.handleDetaillee} value = {this.state.descriptionDetaillee}
                    />

                    <DropDownPicker 
                    items={[
                        {label: 'U-coins ', value: 'U-COINS', untouchable: true, textStyle:{fontWeight: "bold"} },
                        {label: '1', value: '1', parent: 'U-COINS'},
                        {label: '2', value: '2', parent: 'U-COINS'},
                        {label: '3', value: '3', parent: 'U-COINS'},
                        {label: '4', value: '4', parent: 'U-COINS'},
                        {label: '5', value: '5', parent: 'U-COINS'},
                        {label: '6', value: '6', parent: 'U-COINS'},
                        {label: '7', value: '7', parent: 'U-COINS'},
                        {label: '8', value: '8', parent: 'U-COINS'},
                        {label: '9', value: '9', parent: 'U-COINS'},
                        {label: '10', value: '10', parent: 'U-COINS'},

                       
                    ]}
                    
                    style={styles.textinputcoin}
                    itemStyle={{justifyContent: 'flex-start'}}
                    dropDownStyle={{backgroundColor: 'white',  
                    borderWidth: 1,
                    borderColor: '#000000',
                    borderRadius: 10,
                    marginTop: 12,
                    marginRight: 25,
                    marginLeft: 25,
                    width: "88%",
                    alignSelf: "center"}}
                    onChangeItem={item => this.setState({ ucoins: item.value})}
                    placeholder="Selectionnez combien de U-COINS voulez-vous y dédier "
                    labelStyle={{
                        fontSize: 14,
                        textAlign: 'left',
                    }}
                    />
                    <View >
                        <TouchableOpacity style={styles.boutton} onPress={() => {
                            this.sendAskHelp()
                            //this.props.navigation.goBack();
                        }}> <Text style={{fontWeight: "bold", color:"white"}}>Envoyer</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.boutton} onPress={() => this.reset()}><Text style={{fontWeight: "bold", color:"white"}}>Annuler </Text></TouchableOpacity>
                    </View>
                    
                
            </SafeAreaView>
        )
    }

}

const styles = StyleSheet.create({
    
    main:{
        height:'100%',
        width:'100%'
    },
    nom:{
        width: "100%",
        height: 80,
        textAlign:"center",
        justifyContent: "center",
        fontWeight: "bold",
        borderColor: "green",
        position: "absolute",
        borborderWidth : 3,
        
    },
    photo:{
        width: 70,
        height: 70,
        margin: 5,
        resizeMode: 'strech',
        borderRadius:35,

    },

    barre: {
        marginHorizontal: 20,
        borderWidth: 1,
        borderColor: '#000000',
        borderRadius:10,
        marginTop: 10,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
    },

    textinput1 : {
        height: "100%",
        backgroundColor: 'white',  
        marginHorizontal: 20,
        marginBottom: 15,
        borderWidth: 1,
        borderColor: '#000000',
        paddingHorizontal: 5,
        borderRadius: 10,
        
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0,

      },
      textinputcoin : {
      
        backgroundColor: 'white',  
        marginLeft: 25,
        marginRight: 25,
        marginTop: 10,
        borderWidth: 1,
        borderColor: '#000000',
        padding: 10,
        borderRadius: 10,   
      },
      
      
      textinput : {
      
        backgroundColor: 'white',  
        marginLeft: 25,
        marginRight: 25,
        marginTop: 10,
        borderWidth: 1,
        borderColor: '#000000',
        padding: 10,
        borderRadius: 10,   
      },
      boutton: {
        backgroundColor: '#DC4444',
        alignItems: "center",
        marginTop: 20,  
        marginLeft: 55,
        marginRight: 55,
        height: 35,
        justifyContent: "center",
        color: "red",
        borderRadius: 20,
      
    },


})
export default DemandeAide

