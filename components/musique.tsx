import React, { Component } from "react";
import { StyleSheet, Text, View, ActivityIndicator } from "react-native";
import Search from "../src_musique/components/Search";
import Listing from "../src_musique/components/Listing";
import token from "../src_musique/api/token";
import search from "../src_musique/api/search";
import axios from 'axios';

const PAGE = 20;
/**
 * Component intégrant l'API spotify et permettant de modifier la musique préferée de l'utilisateur
 */

class Musique extends Component {
  constructor(login){
    super(login)
    this.state= {
      login : login,
      songs: [],
      offset: 0,
      query: this.getData(),
      isFetching: false,
      isEmpty: false,
      token: null,
      isTokenFetching: false}
  }
/**
 * Récupère la musique préferée de l'utilisateur dans le back.
 */
  getData(){
    axios.get(`http://localhost:3000/users_att/`)
    .then((response) => {
     this.setState({
       query: response.data.[this.state.login.login].musique.author +" " + response.data.[this.state.login.login].musique.title
     })

   })
  .catch((error)=>{
     console.log(error);
  });
}

  async loadNextPage() {
    const { songs, offset, query, token, isFetching, isEmpty } = this.state;

    if (isFetching || isEmpty) return;

    this.setState({ isFetching: true });
    const newSongs = await search({
      offset: offset,
      limit: PAGE,
      q: query,
      token
    });

    if (newSongs.length === 0) {
      this.setState({ isEmpty: true });
    }

    this.setState({
      isFetching: false,
      songs: [...songs, ...newSongs],
      offset: offset + PAGE
    });
  }

  async refreshToken() {
    this.setState({
      isTokenFetching: true
    });
    const newToken = await token();
    this.setState({
      token: newToken,
      isTokenFetching: false
    });
  }

  async componentDidMount() {
    await this.refreshToken();
    await this.loadNextPage();
  }
/**
 * Modifie la musique préferée de l'utilisateur
 * @param text Recherche entrée par l'utilisateur
 */
  handleSearchChange(text) {
    // reset state
    this.setState(
      {
        isEmpty: false,
        query: text,
        offset: 0,
        songs: []
      },
      () => {
        this.loadNextPage();
      }
    );

  }

  async handleEndReached() {
    await this.loadNextPage();
  }

  render() {
    const { songs, query, isFetching } = this.state;
    return (
      <View style={styles.container}>
        <Search onChange={text => this.handleSearchChange(text)} text={query} />
        {isFetching && songs.length === 0 ? (
          <ActivityIndicator />
        ) : (
          <Listing items={songs} onEndReached={() => this.handleEndReached()} login={this.state.login}/>
        )}
      </View>
    );
  }
}

export default Musique

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "stretch",
    justifyContent: "flex-start",
    borderRadius: 10,
    borderWidth: 1,
    marginHorizontal: 25,
    marginBottom: 15,
    padding: 10
  }
});
