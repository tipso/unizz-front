import * as React from 'react';
import { Text, SafeAreaView, View,TextArea, StyleSheet, Modal,Button,Image, TouchableOpacity,TouchableHighlight, ScrollView, Alert ,FlatList} from 'react-native';
import axios from 'axios';
import DropDownPicker from 'react-native-dropdown-picker';
import styles1 from '../Components/ChatListItems/Styles';

/**
 * Classe pour proposer de l'aide Ucoin
 */
class PropositionAide extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            login : this.props.route.params.login,
            proposition:[],
            matiere:{sciences_sociales:false,economie_gestion:false,mathematiques:false,staps:false,science_pour_la_sante:false,science_vie_terre:false,informatique:false},
            level:{L1:false,L2:false,L3:false,M1:false,M2:false},
            show:false,
            data:{}
        }
    }


    /**
     * Pour envoyer le sujet de la demande
     * @param event le sujet de la demande
     * 
     */
    handleSujet=(event)=>{
        this.setState({
            sujetDeLaDemande:event.target.value
        })
    }

    /**
    * Récupère les données de la matière souhaitée et du niveau d'aide 
    */
    recupHelp=()=>{
        console.log(this.state)
        axios({
            method:'POST',
            url:'http://localhost:3000/coursHelp/',
            data:{
                matiere:this.state.matiere,
                level:this.state.level
            }
        })
         .then((response)=> {
             this.setState({proposition:response.data})
         })
    }

    /**
     * Modifier le niveau d'aide souhaité
     */
    handleChangeValueofLevel = (e) => {
        let newObject = this.state.level;
        newObject[e] = !newObject[e]
        this.setState({
          level: newObject 
        },()=>this.recupHelp())
    }

    /**
     * Modifie la matière souhaitée
     */
    handleChangeValueofMatiere = (e) => {
        let newObject = this.state.matiere;
        newObject[e] = !newObject[e]
        this.setState({
          matiere: newObject 
        },()=>this.recupHelp())
    }

    /**
     * Fonction permettant d'afficher lorsqu'un bouton est selectionné ou non
     * @e représente la matière selectionnée
     */
    adaptStyleMatiere=(e)=>{
        let copyofmatiere=this.state.matiere
        if(copyofmatiere[e]){
            return {
            height: 35,
            width:140,
            alignSelf:'center',
            justifyContent: "center",
            backgroundColor: '#A3D7FF',   
            borderRadius: 20,
            borderWidth: 2,
            borderColor:'black'}
        }
        return {
            height: 35,
            width:140,
            alignSelf:'center',
            justifyContent: "center",
            backgroundColor: 'white',   
            borderRadius: 20,
            borderWidth: 2,
            borderColor:'black',}
    }

    /**
     * Fonction permettant d'afficher lorsqu'un bouton est selectionné ou non
     * @e représente le niveau selectionné 
     */
    adaptStyleLevel=(e)=>{
        let copyofmatiere=this.state.level;
        if(copyofmatiere[e]){
            return {
            height: 35,
            width:140,
            alignSelf:'center',
            justifyContent: "center",
            backgroundColor: '#9EB0B3',   
            borderRadius: 20,
            borderWidth: 2,
            borderColor:'black'}
        }
        return {
            height: 35,
            width:140,
            alignSelf:'center',
            justifyContent: "center",
            backgroundColor: 'white',   
            borderRadius: 20,
            borderWidth: 2,
            borderColor:'black',}
    }

    /**
     * Affiche la proposition d'aide détaillée lorsque l'utilisateur clique  sur une personne  
     * @item représente les  données concernant la personne selectionnée
     * La proposition détaillée permettra à la personne d'acceptezr ou non la demande de l'utilisateur. 
     */
    proposition(item){
        console.log(item)
        this.setState({
            show:true, 
            data:item
        })
    }

    render (){

            return(
                
                <SafeAreaView style={{backgroundColor:'#DC4444', height:"100%"}}>
                    <View style={{height: "15%",marginTop:'5%',backgroundColor:'#DC4444'}}>
                            <Image style ={styles.image} source= {require ('../Images/Ucoin/livre.png')} />
                    </View>
                    <View style={{padding:20,flexDirection:'row',width:'100%',flexWrap:'wrap',justifyContent:'space-around',height:'50%',alignContent:'space-around'}} >
                     
                        <TouchableOpacity style={this.adaptStyleMatiere('sciences_sociales')} onPress={()=>this.handleChangeValueofMatiere('sciences_sociales')}>
                            <Text style={{textAlign: "center"}}>Sciences sociales</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={this.adaptStyleMatiere('economie_gestion')} onPress={()=>this.handleChangeValueofMatiere('economie_gestion')}>
                            <Text style={{textAlign: "center"}}>Sciences sociales</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={this.adaptStyleMatiere('mathematiques')} onPress={()=>this.handleChangeValueofMatiere('mathematiques')}>
                            <Text style={{textAlign: "center"}}>Mathématiques</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={this.adaptStyleMatiere('staps')} onPress={()=>this.handleChangeValueofMatiere('staps')}>
                            <Text style={{textAlign: "center"}}>STAPS</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={this.adaptStyleMatiere('science_pour_la_sante')} onPress={()=>this.handleChangeValueofMatiere('science_pour_la_sante')}>
                            <Text style={{textAlign: "center"}}>Sciences pour la santé</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={this.adaptStyleMatiere('science_vie_terre')} onPress={()=>this.handleChangeValueofMatiere('science_vie_terre')}>
                            <Text style={{textAlign: "center"}}>Sciences de la vie et de la Terre</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={this.adaptStyleMatiere('informatique')} onPress={()=>this.handleChangeValueofMatiere('informatique')}>
                            <Text style={{textAlign: "center"}}>Informatique</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={this.adaptStyleLevel('L1')} onPress={()=>this.handleChangeValueofLevel('L1')}>
                            <Text style={{textAlign: "center"}}>Licence 1</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={this.adaptStyleLevel('L2')} onPress={()=>this.handleChangeValueofLevel('L2')}>
                            <Text style={{textAlign: "center"}}>Licence 2</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={this.adaptStyleLevel('L3')} onPress={()=>this.handleChangeValueofLevel('L3')}>
                            <Text style={{textAlign: "center"}}>Licence 3</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={this.adaptStyleLevel('M1')} onPress={()=>this.handleChangeValueofLevel('M1')}>
                            <Text style={{textAlign: "center"}}>Master 1</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={this.adaptStyleLevel('M2')} onPress={()=>this.handleChangeValueofLevel('M2')}>
                            <Text style={{textAlign: "center"}}>Master 2</Text>
                        </TouchableOpacity>
                    </View>
                    
                    <FlatList 
                        style={{width: '100%',height:'100%',borderRadius:10}}
                        data={this.state.proposition}
                        renderItem={({item})=> (
                            <TouchableOpacity style={styles.button} onPress={()=>this.proposition(item)}>
                                    <Image source={item.pdp[0]} style={styles.photoPDP}/>
                                    <Text style={styles.nom}>{item.name}</Text>
                                    <Text style={styles.topic}>sujet : {item.topic}</Text>
                                <Modal 
                                    transparent={true}
                                    visible={this.state.show}>
                                    <View style={{backgroundColor:"#000000aa", flex:1}}>
                                 
                                        <Text style={styles.textdemande}> {item.demande}</Text>
                                        <Button color="#00BF29" title="Aider" 
                                         onPress={() => {
                                            this.setState({show:false})
                                            }}
                                        />
                                        <Button color="#DC4444" title="Revenir aux propositions" 
                                         onPress={() => {
                                            this.setState({show:false})
                                            }}
                                        />
                                    </View>
                                </Modal>
                            </TouchableOpacity>
                             
                            
                        )}
                        keyExtractor={(item) => item.email}
                    />
                    
                    
                </SafeAreaView>
            )
    }
}

const styles = StyleSheet.create({
    main:{
        height:'100%',
        width:'100%'
    },
    container: {
        height:'100%',
        width: "100%",
    },
    topic:{
        width: "100%",
        height: '100%',
        textAlign:'center',
        paddingLeft:'20%',
        justifyContent:'center',
        borderColor: "green",
        position: "absolute",
        borborderWidth : 3,
        paddingTop:12,
    },
    nom:{
        width: "100%",
        height: '100%',
        textAlign:'left',
        paddingLeft:'20%',
        justifyContent:'center',
        borderColor: "green",
        position: "absolute",
        borborderWidth : 3,
        paddingTop:12,
    },
    textinput : {
        backgroundColor: 'white', 
        borderWidth: 1,
        borderColor: '#000000',
        padding: 10,
        borderRadius: 10,   
    },
    button: {
        backgroundColor: 'white',
        marginTop: 20,  
        marginLeft: '5%',
        marginRight: '5%',
        height: 50,
        justifyContent: "center",
        color: 'white',
        borderRadius: 20,
    },
    photoPDP:{
        width:50,
        height: 50,
        margin: 5,
        resizeMode: 'strech',
        borderRadius:25,
    },
    image: {
        width: "100%",
        height: "100%",
        resizeMode : 'contain',
        position: "relative",
        paddingTop:'10%',
        justifyContent: "center",
        alignItems: "center",
    },
    text : {
        height: "20%",
        width:"80%",
        backgroundColor: 'white',  
        borderWidth: 1,
        borderColor: '#000000',
        borderRadius: 10,
        alignSelf:'center'  
    },
    textdemande : {
        height: "80%",
        width:"80%",
        backgroundColor: 'white',  
        borderWidth: 1,
        borderColor: '#000000',
        borderRadius: 10,
        alignSelf:'center'  
    },
})

export default PropositionAide