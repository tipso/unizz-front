import React from 'react'
import { StyleSheet, View, Text, Image,TouchableOpacity,Modal,Button } from 'react-native'
import axios from 'axios';
import { createStackNavigator } from "@react-navigation/stack";
import VisiteProfil from "./VisiteProfil";
import { NavigationContainer } from '@react-navigation/native';
import Navigation from "./Navigation";



var users
const data = axios.get("http://localhost:3000/users_att",{headers:{ "x-access-token": localStorage.getItem("token")}})
  .then((response) => {
    users=response.data})

    /**
     * component pour récuperer les informations issues des notifications 
     */
    class ProfilNotif extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          //login:login.login,
          data: [],
          donnee:"",
          nav:false,
          test: this.getphotoprofil(),
        };
    }
    /**
     * Recois toutes les données des utilisateurs
     */

    getphotoprofil=()=>{
        axios({
            method:'GET',
            url:"http://localhost:3000/users_att",
        }).then((response)=>{
            this.setState({
                donnee:response.data
            })
        })
    }
    /**
     * Recupere la photo de profil de l'utilisateur
     * @param item information de l'utilisateur 
     */

    getPdp(item){
        if(this.state.donnee.[item] ==undefined || this.state.donnee.[item].pdp ==undefined){
            return ""
        }else{
             return this.state.donnee.[item].pdp.[0]
        }
     
     }
 

  render() {
    
    
      const uti = this.props.uti[0];
      
    
      return (
        
      
        <TouchableOpacity style={styles.main_container} onPress={()=> this.setState({nav:true})}>
          <Image
            style={styles.image}
            source={this.getPdp(uti.id)}
          />
          <View style={styles.content_container}>
            <View style={styles.header_container}>
              <Text style={styles.title_text}>{uti.firstname}{' ' + uti.name}</Text>
            </View>
            <View style={styles.description_container}>
              <Text style={styles.description_text}>Vous a {uti.Notif} !</Text>
              <Text style={styles.date_text}>{uti.time} à {uti.heure}</Text>
            </View>
          </View>
          <Modal 
                transparent={true}
                visible={this.state.nav}
          >
            <View style={{height:"100%"}}>
            <TouchableOpacity style={{backgroundColor: "white", height: "5%"}}  onPress={() => {this.setState({nav:false})}}><Text style={{fontWeight: "bold", fontSize: 22}}> &#8656; Retour </Text> </TouchableOpacity>

            <VisiteProfil don ={[this.props.uti[0].id,this.props.uti[1],this.state.nav ]} />
            </View>
            
          </Modal>
        </TouchableOpacity>  
      )
      

   
    
  }
}







const styles = StyleSheet.create({
  main_container: {
    height: 80,
    flexDirection: 'row'
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor:'white',
    marginTop:5
  },
  content_container: {
    flex: 1,
    margin: 5
  },
  header_container: {
    flex: 3,
    flexDirection: 'row',
    marginLeft:5
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 5,
    alignItems:'center',
    paddingTop:'10'
  },
  vote_text: {
    fontWeight: 'bold',
    fontSize: 26,
    color: '#666666'
  },
  description_container: {
    flex: 7
  },
  description_text: {
    fontStyle: 'italic',
    color: '#666666',
    marginLeft:7
  },
  date_container: {
    flex: 1,
  },
  date_text: {
    textAlign: 'right',
    fontSize: 14,
  }
})

export default ProfilNotif