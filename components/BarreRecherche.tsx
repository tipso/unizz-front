import React from "react";
import {SafeAreaView,View,FlatList,Text,StyleSheet,TouchableOpacity, TextInput, Image,Modal,Button  } from "react-native";
import axios from "axios";
import { Searchbar } from "react-native-paper";
import Separator from '../src_musique/components/Separator';
import VisiteProfil from './VisiteProfil';

/**
 * Barre de recherche d'utilisateurs par nom et prénom.
 */
class BarreRecherche extends React.Component{
    constructor(login){
        super(login);
        this.state={
            login:this.props.route.params.login,
            data: [],
            name:"",

            donnees:"",
            test: this.getphotoprofil(),
            nav:false,
            id:""
            
        }
    }



    renderItemComponent=(itemData)=>{
        <TouchableOpacity>
            <Text>{itemData}</Text>
        </TouchableOpacity>
    }
/**
 * Récupère le texte entré dans la barre de recherche
 * @param event texte entré dans la barre de recherche
 */
    handleName =async (event) => {
        this.setState({
            name: event.target.value
        },()=>{
            this.getnameandfirstname()
        })
        
    }

    /**
     * Récuperer la photo de profil du profil recherché
     */
    getphotoprofil=()=>{
        axios({
            method:'GET',
            url:"http://localhost:3000/users_att",
        }).then((response)=>{
            this.setState({
                donnees:response.data
            })
        })
    }

    /**
     * Récuperer le nom à rechercher
     */
    getnameandfirstname=()=>{
        axios({
            method:'POST',
            url:'http://localhost:3000/getusername',
            data:{
                name:this.state.name
            }
        }).then((response) =>{
            this.setState({data:response.data});
        },()=>{
            this.getphotoprofil()
        })
    } 
    
    /**
     * Récuperer la photo de profil à rechercher
     */
    getPdp(item){
       if(this.state.donnees.[item] ==undefined || this.state.donnees.[item].pdp ==undefined){
           return ""
       }else{
            return this.state.donnees.[item].pdp.[0]
       }
    }

    /**
     * Affiche le profil de la personne
     */
    goToProfil=(id)=>{
        this.setState({nav:true, id: id});
        
        
    }
    render(){
        return( 
            <SafeAreaView style={{backgroundColor:'#DC4444', height:"100%"}}>
                <Text style={{margin: 15, textAlign: "center", fontWeight: "bold", fontSize: 22, color:"#111", textDecorationLine: "underline"}}>Rechercher une personne </Text>
                <Searchbar style={styles.barre} placeholder="Rechercher une personne" onChange={this.handleName} value={this.state.name}/>
                <FlatList style ={styles.textinput1}
                    data={this.state.data}
                    renderItem={({item})=> (
                        <View style={styles.main}>
                            <Image style={styles.photo}
                                source={this.getPdp(item.email)}
                            />
                            <TouchableOpacity style={styles.nom} onPress={()=>this.goToProfil(item.email)}><Text style={{fontWeight: "bold", fontSize:18}}>{item.name} {item.firstname}</Text></TouchableOpacity>
                        </View>
                        
                    )}
                    ItemSeparatorComponent={() => <Separator />}
                />
                <Modal 
                transparent={true}
                visible={this.state.nav}
          >
            <View style={{height:"100%"}}>
            <TouchableOpacity style={{backgroundColor: "white", height: "5%"}}  onPress={() => {this.setState({nav:false})}}><Text style={{fontWeight: "bold", fontSize: 22}}> &#8656; Retour </Text> </TouchableOpacity>

            <VisiteProfil don ={[this.state.id,this.state.login,this.state.nav ]} />
            </View>
            
          </Modal>
                
            </SafeAreaView>
        )

    }
    
}

export default BarreRecherche

const styles = StyleSheet.create({
    
    main:{
        height:'100%',
        width:'100%'
    },
    nom:{
        width: "100%",
        height: 80,
        textAlign:"center",
        justifyContent: "center",
        fontWeight: "bold",
        borderColor: "green",
        position: "absolute",
        borborderWidth : 3,
        
    },
    photo:{
        width: 70,
        height: 70,
        margin: 5,
        resizeMode: 'strech',
        borderRadius:35,

    },

    barre: {
        marginHorizontal: 20,
        borderWidth: 1,
        borderColor: '#000000',
        borderRadius:10,
        marginTop: 10,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
    },

    textinput1 : {
        height: "100%",
        backgroundColor: 'white',  
        marginHorizontal: 20,
        marginBottom: 15,
        borderWidth: 1,
        borderColor: '#000000',
        paddingHorizontal: 5,
        borderRadius: 10,
        
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0,

      }


})

