import React from 'react';
import { StyleSheet, Text, Image, View, Button, TextInput,TouchableOpacity,ImageBackground, TouchableWithoutFeedbackBase, AppState} from 'react-native';
import axios from 'axios';
import Inscription from './Inscription'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Nav from './Nav'
import RecoveredEmail from './FogotPassword';



/**
 * Component de connexion
 */
class Connexion extends React.Component {

    constructor(props){
        super(props);
        this.state= {login:"", password:""}
    }

    /**
     * Pour envoyer le login au back
     * @param event le login
     * 
     */
    handleLogin = (event) => {
        this.setState({
            login: event.target.value
        })
    }

    /**
     * Pour envoyer le mot de passe au back
     * @param event le mot de passe
     * 
     */
    handlePassword =(event) => {
        this.setState({
            password: event.target.value
        })
    }

    /**
     * Pour vérifier si le mot de passe est correct
     * et envoi au back 
     */
    checkLogin = () => axios({
        method: 'post',
        url: 'http://localhost:3000/login',
        data: {
          mail: this.state.login,
          password:this.state.password
        }   
    }  
    ).then((response) =>{

        if (response.data.auth){
            localStorage.setItem("token",response.data.token);            
            this.props.navigation.navigate('Nav', {login: response.data.userId});
        }
    });

    render(){
        return(
            
            <View style = {styles.main_container} > 
                <View style={{flex:2}}>
                    <Image style ={styles.image_unizz} source= {require ('./Images/un.png')} />
                </View>
                
                <View style = {{justifyContent: 'center', flex:2}}> 
                    <TextInput style = {[styles.textinput1]}  placeholder = "Identifiant" onChange = {this.handleLogin} value = {this.state.login}/>
                    <TextInput secureTextEntry={true} style = {styles.textinput2 } placeholder = "Mot de passe" onChange = {this.handlePassword} value = {this.state.password}/>
                    <TouchableOpacity style={styles.bouton1} onPress= {this.checkLogin}>
                        <Text style={{fontWeight: "bold"}}>Connexion</Text>
                    </TouchableOpacity >
                </View>
                <View style = {{backgroundColor:'#DC4444', flex:1, marginTOP:5}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('RecoveredEmail')}> 
                        <Text style ={styles.text_mdp}  >Mot de passe oublié ?</Text>
                    </TouchableOpacity>
                </View>  
                <View style = {{backgroundColor:'white', flex:1}}>
                    <Text style ={styles.text_inscription}>Vous n'êtes pas encore inscrit ?</Text>
                    <TouchableOpacity style={styles.bouton1} onPress={()=> this.props.navigation.navigate('Inscription')}>
                        <Text style={{fontWeight: "bold"}}>S'inscrire</Text>
                    </TouchableOpacity>
                </View>              
            </View>
        )
    }
}

const Stack = createStackNavigator()
export default function MyStack() {
    return (
        <Stack.Navigator>
          <Stack.Screen name="Connexion" component={Connexion}  options={{headerShown: false}}/>
          <Stack.Screen name="Inscription" component={Inscription} options={{title: '', headerStyle: {
            height: 40}}} />
          <Stack.Screen name="Nav" component={Nav} options={{headerShown: false}}/>
          <Stack.Screen name="RecoveredEmail" component={RecoveredEmail} options={{title: '', headerStyle: {
            height: 40}}} />
        </Stack.Navigator>
      );
}

const styles = StyleSheet.create ({
    main_container : {
        height:'100%',
        backgroundColor: '#DC4444',
        flex:1,
    },

    image_unizz : {
        alignSelf: "center",
        height: 200,
        width: 225,
        resizeMode: "contain",
        position: "absolute",
        bottom: "0%",  
    },

    textinput1 : {
      backgroundColor: 'white',  
      marginLeft: 35,
      marginRight: 35,
      marginTop: 15,
      height: 50,
      borderWidth: 1,
      borderColor: '#000000',
      padding: 10,
      borderRadius: 10
    },

    textinput2: {
      backgroundColor: 'white',  
      marginTop:15,
      marginLeft: 35,
      marginRight: 35,
      marginBottom:10,
      height: 50,
      borderWidth: 1,
      borderColor: '#000000',
      padding: 10, 
      borderRadius: 10
    },

    bouton1: {
        backgroundColor: '#DDDDDD',
        alignItems: "center",
        marginTop: 15,  
        marginLeft: 55,
        marginRight: 55,
        height: 35,
        justifyContent: "center",
        color: "red",
        borderRadius: 20
    },

    text_mdp: {
        marginTop:10,
        textAlign: 'center',
        textDecorationLine: 'underline'
    },

    text_inscription: {
        paddingTop: 10,
        textAlign: 'center',
    }
})
  
