import * as React from 'react';
import { Text, View, Button, StyleSheet, Image, TouchableOpacity, ScrollView, Alert ,FlatList} from 'react-native';
import axios from 'axios';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Modif from './Modif'
import Musique from './Musique'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import Ionicons from 'react-native-ionicons'


var pdp = require('../Images/Profil/photo_profil.jpg')

/**
 * Component affichant le profil de l'utilisateur, contenant les différents boutons pour modifier son profil.
 */

class Profil extends React.Component {
    constructor(props){
        super(props)
        this.state={
            firstname:this.ftest(),
            age:"",
            login: this.props.route.params.login,
            uri: pdp,
            }
    }
    /**
     * Envoie le paramètre du profil à modifier au component "Modif"
     * @param zone partie du profil à modifier 
     */
    submit(zone){
       
        this.props.navigation.navigate('Modifier', {zone, login: this.state.login, updateText: (newText, para) => this.updateText(newText, para)})
    }

    

    
    /**
     * Fonction test temporaire pour récupérer le nom et age
     * @param
     */
    ftest(){
        axios.get('http://localhost:3000/users',{
            headers:{
                "x-access-token": localStorage.getItem("token"),
            }
        })
        .then((response) => {
            
         let age=response.data.[this.state.login].birthdate.split('/');
         age= 2021 - parseInt(age[2]);
         this.setState({firstname: response.data.[this.state.login].firstname, age:age});
       })
      .catch((error)=>{
         console.log(error);
      });
    }
    /**
     *Demande la permission d'utiliser la caméra pour changer la photo de profil 
     */
    askForPermission = async () => {
        const permissionResult = await Permissions.askAsync(Permissions.CAMERA)
        if (permissionResult.status !== 'granted') {
          Alert.alert('no permissions to access camera!', [{ text: 'ok' }])
          return false
        }
        return true
    }

    
/**
 * Permet de modifier la photo de profil de l'utilisateur en prenant une photo dans sa galerie.
 */
    handlePickAvatar = async () => {
        const hasPermission = await this.askForPermission()
        if (!hasPermission) {
            return
          } else {
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes:ImagePicker.MediaTypeOptions.Images,
                allowsEditing: true,
                quality: 1,
                base64: false,
            });
            if (!result.cancelled){
                axios({
                    method:'POST',
                    url:'http://localhost:3000/images3/'+this.state.login,
                    data:{
                      imgsource: result
                    }
                }).then((response) =>{
                  this.setState({uri:response.data.[0]});
                })
                
            }
          }    
    };
    
    /**
 * Permet de modifier la photo de profil de l'utilisateur en prenant une photo depuis sa caméra.
 */
    getImage= async () => {
        try {
          const res = await  axios.get('http://localhost:3000/pdp/'+this.state.login)
          if (res.data.[0] != undefined) {
            this.setState({uri:res.data.[0]});

          }
        } catch (error) {
          console.log('error:', error);
        }
      }
  
    componentDidMount() {
      this.getImage();
    }

   

   
  
        
        
    

    isauthen = (mon) => axios({
        method: 'get',
        url: 'http://localhost:3000/isauth',
        headers :{
            "x-access-token": localStorage.getItem("token"),
        }
    }
    ).then((response) =>{
        this.submit(mon);
        
        
    });
    
    
    
      
    
    render(){
        return (

            //Vue principale
            <ScrollView style={styles.main_container}>
                
               {/* Vue affichant la photo de profil de l'utilisateur*/}
                <View style={styles.pdp}>
                    <TouchableOpacity style={styles.bouttonpdp} onPress={this.handlePickAvatar}>
                    <Image
                        style={styles.imagepdp}
                        source={this.state.uri}
                    />
                                          
                    </TouchableOpacity>
                    
                    
                </View>

                {/* Vue affichant le prénom et l'age*/}
                <View style={styles.nom}>
                    <Text style={styles.nom}>{this.state.firstname}, {this.state.age} ans</Text>
                </View>

                {/* Vue contenant le boutton pour modifier les photos*/}
                <View style={styles.zones} >
                    <TouchableOpacity style={styles.boutton} onPress={()=> this.isauthen("picture")}>
                        
                    <Image 
                    source={require('../Images/Profil/m_profil.jpg')}
                    style={styles.images}/>                  
                    </TouchableOpacity>
                </View>

                {/* Vue contenant le boutton pour modifier la biogrpahie*/}
                <View style={styles.zones}>
                    <TouchableOpacity style={styles.boutton} onPress={()=> this.isauthen("bio")}>
                    <Image 
                    source={require('../Images/Profil/m_bio.jpg')}
                    style={styles.images}
                    />                   
                    </TouchableOpacity>
                </View>

                 {/* Vue contenant le boutton pour modifier la recherche*/}
                <View style={styles.zones}>
                    <TouchableOpacity style={styles.boutton} onPress={()=> this.isauthen("recherche")}>
                   <Image 
                    source={require('../Images/Profil/m_recherche.jpg')}
                    style={styles.images}/>                  
                    </TouchableOpacity>
                </View>

                 {/* Vue contenant le boutton pour modifier les passions*/}
                <View style={styles.zones}>
                    <TouchableOpacity style={styles.boutton} onPress={()=> this.isauthen("passions")}>
                   <Image 
                    source={require('../Images/Profil/m_passion.jpg')}
                    style={styles.images}/>                  
                    </TouchableOpacity>
                </View>

                 {/* Vue contenant le boutton pour modifier les cours*/}
                <View style={styles.zones}>
                    <TouchableOpacity style={styles.boutton} onPress={()=> this.isauthen("cours")}>
                   <Image 
                    source={require('../Images/Profil/m_cours.jpg')}
                    style={styles.images}/>                  
                    </TouchableOpacity>
                </View>

                 {/* Vue contenant le boutton pour modifier la musique*/}
                <View style={styles.zones}>
                    <TouchableOpacity style={styles.boutton} onPress={()=> this.isauthen("musique")}>
                   <Image 
                    source={require('../Images/Profil/m_musique.jpg')}
                    style={styles.images}/>                  
                    </TouchableOpacity>
                </View>

                {/* Vue contenant le boutton pour accéder aux UCoins*/}
                <View style={styles.zones}>
                    <TouchableOpacity style={styles.boutton} onPress={()=> this.isauthen("Coin")}>
                   <Image 
                    source={require('../Images/Profil/coin.png')}
                    style={styles.images}/>                  
                    </TouchableOpacity>
                </View>

                {/* Vue contenant le boutton pour accéder aux paramètres*/}
                <View style={styles.zones}>
                    <TouchableOpacity style={styles.boutton} onPress={()=> this.isauthen("parametres")}>
                   <Image 
                    source={require('../Images/Profil/parametres.png')}
                    style={styles.images}/>                  
                    </TouchableOpacity>
                </View>

               
                

            </ScrollView>
            
        
        )
    }
}



// Création de la navigation avec Modif.tsx
const Stack = createStackNavigator()
export default function MyStack({route}) {
    return (
        <Stack.Navigator
        screenOptions={{
            headerShown: true,
            cardStyle: { backgroundColor: '#EEEEEE' }
          }}>
          <Stack.Screen name="Profil" component={Profil} options={{headerShown: false}} initialParams={{login: route.params.login}}/>
          <Stack.Screen name="Modifier" component={Modif}  options={{title: '', headerStyle: {
            height: 40, backgroundColor: 'transparent', borderBottomWidth: 0 } }}/>
        </Stack.Navigator>
      );
}




//Styles appliqués aux components au dessus
const styles = StyleSheet.create({
    main_container: {
      flex: 1,
      backgroundColor: '#DC4444',
      padding: 15,
     
    },

    pdp: {
      
      height: 200,
      width: 200,
      marginRight: 'auto',
      marginLeft: 'auto',
      marginTop: 5,
      borderRadius: 100,
    },

    bouttonpdp:{
        
        width: 200,
        height: 200,
        borderRadius: 100,
        borderWidth : 3,
        borderColor: "black"
    },

    imagepdp:{
        width: "100%",
        height: "100%",
        resizeMode : 'stretch',
        borderRadius: 100
    },

    nom:{
        margin: 5,
        fontSize: 26,
        fontWeight: 'bold',
        textAlign: 'center',
    },

    zones:{
      
      width: '100%',
      height: 75,
      marginVertical: 10,
      borderRadius: 20,
      borderWidth: 1,
      borderColor: "black"
   
    },

    images:{
        width: "100%",
        height: '100%',
        resizeMode : 'stretch',
        borderRadius: 20,
        
    },

    boutton:{
        flex: 1,
        height : "100%",
        width: '100%',
        
    },
    avatarPlaceholder:{
        flex:1,
        width:100,
        height: 100,
        backgroundColor:"E1E2E6",
        borderRadius:50,
        marginTop:48,
        justifyContent:"center",
        alignItems:"center"
    },
    avatar:{
        position:"absolute",
        width:120,
        height:120,
        resizeMode : 'stretch',
        borderRadius:50,
        marginLeft:6,
        marginBottom:5
    }

  })


