import React, { Component } from "react";
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Image, Button, Modal, Animated, Dimensions} from "react-native";
import {BackgroundCarousel} from "./BackgroundCarousel";
import axios from 'axios';
import Separator from '../src_musique/components/Separator'

const DEVICE_HEIGHT =Dimensions.get("window").height;

const images = [
  "https://images.freeimages.com/images/small-previews/a0d/autumn-tree-1382832.jpg",
  "https://images.unsplash.com/photo-1485550409059-9afb054cada4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80",
  "https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80",
  "https://images.unsplash.com/photo-1429087969512-1e85aab2683d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80",
  "https://images.unsplash.com/photo-1505678261036-a3fcc5e884ee?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80"
];

var i=0
var usersKey
var users
const data = axios.get("http://localhost:3000/users",{headers:{ "x-access-token": localStorage.getItem("token")}})
  .then((response) => {
    usersKey=Object.keys(response.data)
    users=response.data})

/**
 * Component où sont proposés les profils pouvant être likés et matchés.
 */
class Propositions_Profils extends Component {
  constructor(props){
    super(props)
    this.state ={
        login : this.props.route.params.login,
        photos: images,
        id: "",
        picture:[],
        bio: "",
        recherche: "",
        passions: [],
        cours: "",
        musique:"",
        prenom:"",
        age:"",
        tableau: [],
        personnalite: "",
        showList: false,
        couleur: "#f00",
        likeIn: [],
        likeInNotif: [],
        likeOut: [],
        checkIn: [],
        checkOut: [],
        matchLike: [],
        matchCheck: [],
        masqued: [],
        donnees: "",
        showL : false,
        showC : false,
        showN : false,
        test: data,
        isDisabled: "",
        progress: ""     
    }
}

anim = new Animated.Value(0)




/**
 * Création de l'animation de la barre de 5 secondes qui empêche le défilement de profils trop rapide.
 */
animation(){
  this.anim.setValue(0)
  this.setState({isDisabled: true, progress:0})
  this.onAnimate(); 
  setTimeout(() => this.setState({ isDisabled: false }), 1000);
}
/**
 * Déroulement de l'animation de la barre de 5 secondes qui empêche le défilement de profils trop rapide.
 */
onAnimate = () =>{  
  this.anim.addListener(({value})=> {  
      this.setState({progress: parseInt(value,10)});  
  });  
  Animated.timing(this.anim,{  
       toValue: 100,  
       duration: 900,  
  }).start();  
}  

reset(){
  i =0
  axios.get('http://localhost:3000/users/', {headers:{ "x-access-token": localStorage.getItem("token")}})
  .then((response)=>{
    this.setState({
      checkOut: response.data.[this.state.login].checkOut,
      likeOut: response.data.[this.state.login].likeOut
    })
  })
}

/**
 * Permet de réinitialiser les profils déjà vus
 */
raz(){
  axios({
    method: 'post',
    url: 'http://localhost:3000/raz',
    data: {
      zero: []
    }   
  } )
  .then(()=> window.location.reload(false))
}

/**
 * change la couleur de fond du profil en fonction de sa personnalité
 */
changeColor(){
  if(this.state.personnalite == "Architecte INTJ-A/INTJ-T" || this.state.personnalite == "Logicien INTP-A/INTP-T" || this.state.personnalite == "Commandant ENTJ-A/ENTJ-T" || this.state.personnalite == "Innovateur ENTP-A/ENTP-T"){
    return {
      height: "88%",
      width:"100%",
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: `#e6e6fa`,
      borderWidth:4,
      borderColor:"black",
      borderRadius: 10
    }
  }
  else if(this.state.personnalite == "Avocat INFJ-A/INFJ-T" || this.state.personnalite == "Médiateur INFP-A/INFP-T" || this.state.personnalite == "Protagoniste ENFJ-A/ENFJ-T" || this.state.personnalite == "Inspirateur ENFP-A/ENFP-T"){
    return {
      height: "88%",
      width:"100%",
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: `#C2F9CC`,
      borderWidth:4,
      borderColor:"black",
      borderRadius: 10,
    }
  }
  else if(this.state.personnalite == "Logisticien ISTJ-A/ISTJ-T" || this.state.personnalite == "Défenseur ISFJ-A/ISFJ-T" || this.state.personnalite == "Directeur ESTJ-A/ESTJ-T" || this.state.personnalite == "Consul ESFJ-A/ESFJ-T"){
    return {
      height: "88%",
      width:"100%",
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: `#B8EDF9`,
      borderWidth:4,
      borderColor:"black",
      borderRadius: 10
    }
  }
  else if(this.state.personnalite == "Virtuose ISTP-A/ISTP-T" || this.state.personnalite == "Aventurier ISFP-A/ISFP-T" || this.state.personnalite == "Entrepreneur ESTP-A/ESTP-T" || this.state.personnalite == "Amuseur ESFP-A/ESFP-T"){
    return {
      height: "88%",
      width:"100%",
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: `#F7D88F`,
      borderWidth:4,
      borderColor:"black",
      borderRadius: 10
    }
  }
  else{
    return {
      height: "88%",
      width:"100%",
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: `#EEE`,
      borderWidth:4,
      borderColor:"black",
      borderRadius: 10
    }
  }
}

/**
 * Change le profil affiché
 */
change(){
  i++
  this.forceUpdate() 
  //this.animation() 
}

/**
 * Ouvre la liste d'options supplémentaires
 */
_onPressOpen = () => {
  this.setState({showList: true})
}

/**
 * Ferme la liste d'options supplémentaires
 */
_onPressClose = () => {
  this.setState({showList :false})
}

/**
 * Fonction déclenchée lors de l'appui sur le bouton d'options supplémentaires.
 * Appelle la fonction "_onPressOpen" ou "_onPressClose"
 */
_onPressCombinated =() =>{
  if ( this.state.showList === false ){
    this._onPressOpen()
  }
  else {
    this._onPressClose()
  }
}

// Ajout de token
/**
 * Récupère les données de l'utilisateur affiché depuis le back.
 * @param i id de l'utilisateur affiché
 */
getData(i){
  axios.get(`http://localhost:3000/users_att`,{headers:{ "x-access-token": localStorage.getItem("token")}})
  .then((response) => {
   this.setState({
      picture: response.data.[i].picture,
      bio: response.data.[i].bio,
      recherche: response.data.[i].recherche,
      passions: response.data.[i].passions,
      cours: response.data.[i].cours,
      musique: response.data.[i].musique,      
   })
   if(response.data.[i].picture.length == 0){
     this.setState({picture: images})
   }
   
 })
.catch((error)=>{
   console.log(error);
})
.then(()=> this.animation())
axios.get(`http://localhost:3000/users`,{headers:{ "x-access-token": localStorage.getItem("token")}})
  .then((response) => {
    let age=response.data.[i].birthdate.split('/')
    age= 2021 - parseInt(age[2])
    this.setState({
      donnees: response.data,
      prenom: response.data.[i].firstname,
      age:age,
      id: response.data.[i].email,
      personnalite : response.data.[i].personnalities,
      likeIn: response.data.[this.state.login].likeIn,
      likeOut: response.data.[this.state.login].likeOut,
      checkIn: response.data.[this.state.login].checkIn,
      checkOut: response.data.[this.state.login].checkOut,
      matchLike: response.data.[this.state.login].matchLike,
      matchCheck: response.data.[this.state.login].matchCheck, 
      masqued : response.data.[this.state.login].masqued   
       })

 })
.catch((error)=>{
   console.log(error);
})

}

/**
 * Envoie un like à l'utilisateur affiché.
 */
like(){
  axios({
    method: 'post',
    url: 'http://localhost:3000/users_like/'+this.state.login,
    data: {
      likeOut: usersKey[i],
      likeIn: this.state.login,
      
    }   
  } )
  
  .then(()=> this.matchLike())
  .then(()=> this.change())
  .catch(()=>{
    this.setState({showN: true})
  })
}

/**
 * Lors d'un like envoyé, vérifie sur le like est réciproque.
 */
matchLike(){
  if (this.state.donnees.[this.state.id].likeOut.includes(this.state.login)){
    axios({
      method: 'post',
      url: 'http://localhost:3000/users_likeM/'+this.state.login,
      data: {
        match: this.state.id
      }   
    } )  
    this.setState({showL: true})    
  }
}

/**
 * Envoie un check à l'utilisateur affiché
 */
check(){
  axios({
    method: 'post',
    url: 'http://localhost:3000/users_check/'+this.state.login,
    data: {
      checkOut: usersKey[i],
      checkIn: this.state.login 
    }   }  )
  .then(()=> this.matchCheck())
  .then(()=> this.change())
}

/**
 * Lors d'un check envoyé, vérifie sur le check est réciproque.
 */
matchCheck(){
  if (this.state.donnees.[this.state.id].checkOut.includes(this.state.login)){
    axios({
      method: 'post',
      url: 'http://localhost:3000/users_checkM/'+this.state.login,
      data: {
        match: this.state.id
      }   
    } )  
    this.setState({showC: true})    
  }
}

masquer(){
  console.log("t")
  axios({
    method: 'post',
    url: 'http://localhost:3000/masquer/'+this.state.login,
    data: {
      masque: usersKey[i],
    }   
  } )
  .then(()=> this.change())

}

signaler(){
  axios({
    method: 'post',
    url: 'http://localhost:3000/signaler/'+this.state.login,
    data: {
      signaled: usersKey[i],
    }   
  } )
  .then(()=> this.masquer())

}

  render() {
    this.changeColor()
    
    if (i<usersKey.length) {

      if (this.state.login == usersKey[i] || users.[this.state.login].likeOut.includes(usersKey[i]) || users.[this.state.login].checkOut.includes(usersKey[i]) || this.state.checkOut.includes(usersKey[i]) || this.state.likeOut.includes(usersKey[i]) || this.state.masqued.includes(usersKey[i])){
        i++
        this.forceUpdate()
      }
      
      else if(this.state.id != usersKey[i]){
        this.getData(usersKey[i])
      }

      let passions =""
      for (const [key, value] of Object.entries(this.state.passions)) {
        passions += value.label + "\n"
      }
      let recherche
      switch(this.state.recherche){
        case "value1" : recherche = "Amour"; break;
        case "value2" : recherche = "Amitié"; break;
        case "value3" : recherche = "Amitié et amour"; break
      }

      return (
        <View style={{width:"100%", height:"100%", backgroundColor: "#DC4444", padding: 15, position:'absolute'}}>
         
        <View style={this.changeColor()}>
          <View style={styles.prenomage}>
          <Text style = {{fontSize: 20, fontWeight: "bold", textAlign: "center", width: "100%", marginRight: "50%"}}>{this.state.prenom}</Text>
          <Text style = {{fontSize: 20, fontWeight: "bold", textAlign: "right", width: "100%", position: "absolute", marginRight: "10%"}}>{this.state.age} ans</Text>
                 
          </View>
             
          <View style={styles.carousel}>
            
            <View style={styles.background}>
            <BackgroundCarousel images={this.state.picture} style={{flex:1, resizeMode:"contain"}}/>
            </View>
            
            <View>
            
            
            <Modal 
                transparent={true}
                visible={this.state.showC}
                >
                  <View style={{backgroundColor:"#000000aa", flex:1}}>
                    <View style={{backgroundColor:"#ffffff", margin:50, padding:30, borderRadius:10, top: 200}}>
                        <Text style={{textAlign: "center"}}>Bravo, c'est un check match !{"\n"}Vous pouvez désormais discuter ensemble !</Text>
                        <Text> </Text>
                        <Button color="#DC4444" title="D'accord !" 
                        onPress={() => {
                            this.setState({showC:false})
                            }}
                        />
                        
                    </View>
                  </View>
                </Modal>
                <Modal 
                transparent={true}
                visible={this.state.showL}
                >
                  <View style={{backgroundColor:"#000000aa", flex:1}}>
                    <View style={{backgroundColor:"#ffffff", margin:50, padding:30, borderRadius:10, top: 200}}>
                        <Text style={{textAlign: "center"}}>Bravo, c'est un like match !{"\n"}Vous pouvez désormais discuter ensemble !</Text>
                        <Text> </Text>
                        <Button color="#DC4444" title="D'accord !" 
                        onPress={() => {
                            this.setState({showL:false})
                            }}
                        />
                        
                    </View>
                  </View>
                </Modal>

                <Modal 
                transparent={true}
                visible={this.state.showN}
                >
                  <View style={{backgroundColor:"#000000aa", flex:1}}>
                    <View style={{backgroundColor:"#ffffff", margin:50, padding:30, borderRadius:10, top: 200}}>
                        <Text style={{textAlign: "center"}}>Vous ne pouvez plus liker aujourd'hui !{"\n"}Revenez demain pour pouvoir liker de nouveau !</Text>
                        <Text> </Text>
                        <Button color="#DC4444" title="D'accord !" 
                        onPress={() => {
                            this.setState({showN:false})
                            }}
                        />
                        
                    </View>
                  </View>
                </Modal>
            </View>
            
           
          </View>
  
          <ScrollView style={styles.infos}>
  
            <Text style ={styles.title}>Biographie :</Text>
            <Text >{this.state.bio}</Text>
            <Separator/>
  
            <Text style ={styles.title}>Recherche sur Unizz :</Text>
            <Text >{recherche}</Text>
            <Separator/>
  
            <Text style ={styles.title}>Passions :</Text>
            <Text >{passions}</Text>
            <Separator/>
  
            <Text style ={styles.title}>Cours :</Text>
            <Text >{this.state.cours}</Text>
            <Separator/>
  
            <Text style ={styles.title}>Musique préferée :</Text>
            <View style = {styles.musique}>
              <Image source={this.state.musique.imageUri} style={styles.imgmusique}/>
              <Text >{this.state.musique.author + "\n"}{this.state.musique.title}</Text>
            </View>
          </ScrollView>
    
          
        </View>
        <View style={styles.cont_bouttons}>
          
        <Modal 
                transparent={true}
                visible={this.state.isDisabled}
                animationType={"slide"}
                >
                  <View style ={{top:"87%", justifyContent:"center", width: "100%", height:"13%", backgroundColor: "rgba(255, 255, 255, 0.6)"}}>
                  <View style={styles.barre}>  
            <Animated.View  
                style={[  
                    styles.inner,{width: this.state.progress +"%"},  
                ]}  
            />
            </View>
                  </View>
                  
              </Modal>
            <TouchableOpacity  disabled={this.state.isDisabled} style={styles.bouttons} onPress={()=> this.like()}>
              <Image style={styles.image}
              source={require("../Image/coeur.png")}></Image>
            </TouchableOpacity>
            <TouchableOpacity disabled={this.state.isDisabled} style={styles.bouttons} onPress={()=> this.check()}>
              <Image style={styles.image}
              source={require("../Image/check.png")}></Image>
            </TouchableOpacity>
            <TouchableOpacity  disabled={this.state.isDisabled} style={styles.bouttons} onPress={()=> this.change()}>
              <Image style={styles.image}
              source={require("../Image/croix.png")}></Image>
  
            </TouchableOpacity>
  
          </View>
          <View style={{position: "absolute", top:"3.8%", left:'7%'}}>
          <TouchableOpacity  style={styles.plus} onPress={ this._onPressCombinated}>
                <Image style={{width: 30, height:45, resizeMode:"contain"}}
              source={require("../Image/ellipsis.png")}></Image>
              </TouchableOpacity>
              {(this.state.showList)
               &&
               <View style={styles.viewButton}>
                <TouchableOpacity
                style={styles.buttonSignal} 
                onPress={() => this.signaler()}>
                <Text>Signaler</Text>
                </TouchableOpacity>
                <TouchableOpacity
                style={styles.buttonSignal} 
                onPress={() => this.masquer()}>
                <Text>Masquer</Text>
                </TouchableOpacity>
                <TouchableOpacity
                style={styles.buttonSignal} 
                onPress={() => alert('utilisateur partagé')}>
                <Text>Partager</Text>
                </TouchableOpacity>
               </View>
               }
          </View>
        </View>
      );
    }
    else{
      return (
        <View style={{width:"100%", height:"100%", backgroundColor: "#DC4444", padding: 30, borderWidth: 1}}>
          <View style={{flex:1, alignItems: 'center', justifyContent: 'center', backgroundColor: "white", borderRadius: 10}}>
          <Text style={{fontSize:22, fontWeight: "bold", marginBottom: 20 }}>Quel dommage !</Text>
          <Image style={{resizeMode:"contain", height:200, width:200}}source={require("../Images/Propositions/visage-decu.png")}></Image>
          <Text style={{fontSize:22, fontWeight: "bold", marginTop: 20, marginBottom: 50 }}>Vous avez vu tous les profils !</Text>
          <Button color="#DC4444" title={"Revoir ceux dont je \n ne suis pas intéressé"} onPress={() => this.reset()}/>
          <Button title="TOUT REMETTRE a 0 /!\" onPress={()=> this.raz()}/>
                 
        </View>
        </View>
        
      )
    }
    
  }
};
export default Propositions_Profils

const styles = StyleSheet.create({
  barre: {  
    width: "100%",  
    height: 20,  
    padding: 3,  
    borderColor: "black",  
    borderWidth: 3,  
    borderRadius: 30,  
    justifyContent: "center",  
    backgroundColor : "white",
  },  
  inner:{  
    width: "100%",  
    height: 10,  
    borderRadius: 15,  
    backgroundColor:"red",  
  },  

  background:{
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    borderRadius: 10,
  },

  plus: {
    width: "15%",
    left:5,
    top: -10,
  },

  carousel: {
    height: "50%",
    width: '95%',
    borderRadius: 10,
    marginTop: 5,
    borderWidth: 2,
    
  },

  infos: {
    height: "50%",
    width: '100%',
    padding: 10,
  },

  cont_bouttons: {
    width: '100%',
    height: '12%',
    justifyContent: 'space-around',
    flexDirection:"row",
    marginTop: 10
    
  },

  bouttons: {
    justifyContent: 'center',
  },

  image:{
    width: 70,
    height: 70,
    resizeMode:"contain",
    alignSelf:"center",
    borderWidth: 2,
    borderRadius: "50%",
    
  },

  imgmusique: {
    width: 100,
    height: 100,
    marginRight: 10,
    borderRadius: 10
  },

  musique: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "rgba(255, 255, 255, 0.8)",
    padding: 5,
    margin: 5,
    borderRadius: 10
  },


  viewButton:{
    height:"100%",
    width:"100%",
    marginLeft:1,
    borderRadius:10,
    backgroundColor: "rgba(0, 0, 0, 0.3)",
    padding: 10,
    top: -10,
    alignItems: "center"
  
  },
  buttonSignal :{
    backgroundColor:"white",
    alignItems:"center",
    borderRadius:10,
    width:"100%",
    height:"60%",
    borderWidth:2,
    borderColor:"red",
    margin: 5
    

  },

  title:{
    fontSize: 18,
    fontWeight: "bold",
    textDecorationLine: "underline"
  },

  prenomage:{
    backgroundColor: "white",
    borderRadius: 5,
    alignItems: 'center',
    marginTop: 10,
    width: "95%",
  },

  
})