import * as React from "react";
import {StyleSheet,View,ScrollView,Dimensions,Image} from "react-native";

const DEVICE_WIDTH =Dimensions.get("window").width;
/**
 * component permettant d'afficher le carousel d'image 
 */

class BackgroundCarousel extends React.Component{
    scrollRef =React.createRef();
    constructor(props){
        super(props);
    }


   
    render(){
        const {images} = this.props;
        return(
            <View style={{height:"100%", width:"100%"}}>
                <ScrollView horizontal pagingEnabled>
                    {images.map(image => (
                        <Image
                         
                          source={image}
                          style={styles.backgroundImage}/>
                    ))}
                </ScrollView>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    backgroundImage:{
        height: "100%",
        width: 0.95*(DEVICE_WIDTH)-30,
        resizeMode: "stretch",
        backgroundColor : "rgba(0, 0, 0, 0.2)",
        borderRadius: 10,
        
        borderColor:"black"
      

    },
    
});

export {BackgroundCarousel};