import React, { Component, useState } from "react";
import Search from "../src_musique/components/Search";
import Listing from "../src_musique/components/Listing";
import token from "../src_musique/api/token";
import search from "../src_musique/api/search";
import axios from 'axios';
import {StyleSheet, Text, Image, ScrollView, View, SafeAreaView, Button,FlatList,  TextInput,TouchableOpacity,ImageBackground, TouchableWithoutFeedbackBase, AppState} from 'react-native';
import ProfilNotif from "./ProfilNotif";
import Separator from '../src_musique/components/Separator';

var i=0
var usersKey
var users
const data = axios.get("http://localhost:3000/users",{headers:{ "x-access-token": localStorage.getItem("token")}})
  .then((response) => {
    usersKey=Object.keys(response.data)
    users=response.data})

/**
 * Component pour afficher toutes les notifications 
 */
class NotificationsScreen extends Component {
  constructor(props){
    super(props)
    this.state= {
        login : this.props.route.params.login,
        likeInNotif: "",
    };    
  };

/**
 * Récupération des Likes et Checks du Back 
 */
  getLikeIn= async () => {
    try {
      const res = await  axios.get('http://localhost:3000/users/'+this.state.login)
      if (res) {
        this.setState({likeInNotif:res.data});
        
      }
    } catch (error) {
      console.log('error:', error);
    }
  }

  componentDidMount() {
    this.getLikeIn();
  }

  render(){      
    return (
        <SafeAreaView style={{backgroundColor:'#DC4444', height:"100%"}}>
                <Text style={{margin: 15, textAlign: "center", fontWeight: "bold", fontSize: 22, color:"#111", textDecorationLine: "underline"}}>Centre de notifications</Text>
                <FlatList style={styles.textinput1}
                    data={this.state.likeInNotif}
                    renderItem={({item}) => <ProfilNotif uti={[item,this.state.login]}
                    />}
                    ItemSeparatorComponent={() => <Separator />} 
                />
        </SafeAreaView>
      );
  }

}
//Styles appliqués au component
  const styles = StyleSheet.create({
    container: {
      justifyContent: 'flex-start',
      height: "100%",
      backgroundColor: 'white',  
      marginHorizontal: 20,
      marginBottom: 1,
      borderWidth: 1,
      borderColor: '#000000',
      borderRadius: 10,
      textAlign: "left",
    },
    nom:{
        margin: 10,
        fontSize: 26,
        fontWeight: 'bold',
        textAlign: 'center',
    },

    textinput1 : {
      height: "100%",
      backgroundColor: 'white',  
      marginHorizontal: 20,
      margin: 15,
      borderWidth: 1,
      borderColor: '#000000',
      borderRadius: 10,
      paddingHorizontal: 5

    }
  });

  export default  NotificationsScreen;


