import * as React from 'react';
import { Text, View, StatusBar, Dimensions } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Constants from 'expo-constants'
import Propositions_Profils from './Propositions_Profils';
import Profil from './Profil'
import MessagesScreen from './MessagesScreen'
import NotificationsScreen from './NotificationsScreen'
import ChatsScreen from '../screens/ChatsScreen';
import ChatRoomScreen from '../screens/ChatRoomScreen';
import BarreRecherche from './BarreRecherche';

const DEVICE_HEIGHT =Dimensions.get("window").height;




  
  
  //Fin écrans temporaires
  

  
  const Tab = createMaterialTopTabNavigator();
  
  export default function MyTabs({route}) {
    return (
      
      <Tab.Navigator
        initialRouteName="Profils"
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;
  
            if (route.name === 'Recherche') {
              iconName = focused ? 'search': 'search-circle';
            } else if (route.name === 'Notifications') {
              iconName = focused ? 'notifications' : 'notifications-circle';
            }else if (route.name === 'Profils') {
              iconName = focused ? 'albums-sharp' : 'albums';
            }else if (route.name === 'Messages') {
              iconName = focused ? 'chatbubble-ellipses' : 'chatbubble';
            }else if (route.name === 'Profil') {
              iconName = focused ? 'person' : 'person-circle';
            }
  
            return <Ionicons name={iconName} size={40} color={color} />;
          },
          
          
        })
        
      }
        tabBarOptions={{
          activeTintColor: '#DC4444',
          inactiveTintColor: '#555',
          style: { backgroundColor: 'white', marginTop: Constants.statusBarHeight},
          showIcon: true,
          showLabel: false,
          indicatorStyle: {opacity : 0},
          tabStyle: {margin: 0, padding: 0, height: 55},
          iconStyle:{width: 55, height: 55}
          
        }}
  
      >
        <Tab.Screen
          name="Recherche"
          component={BarreRecherche}
          initialParams = {{login: route.params.login}}
          
        />
        <Tab.Screen
          name="Notifications"
          component={NotificationsScreen}
          initialParams = {{login: route.params.login}}
          
        />
        <Tab.Screen
          name="Profils"
          component={Propositions_Profils}
          initialParams = {{login: route.params.login}}
          
        />
         <Tab.Screen
          name="Messages"
          component={ChatsScreen}
          initialParams = {{login: route.params.login}}
          
        />

        <Tab.Screen
          name="Profil"
          component={Profil}
          initialParams = {{login: route.params.login}}
          
        />
      </Tab.Navigator>
    );
  }
