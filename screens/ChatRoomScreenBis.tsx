import React, { useEffect, useState, useRef } from "react";
import { StyleSheet, Text, View, TextInput,Button, FlatList,Platform, KeyboardAvoidingView } from "react-native";
//import { Button } from "react-native-paper";
import io from "socket.io-client";
import { Bubble, GiftedChat, Send } from 'react-native-gifted-chat'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialcommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
/**
 * Component des bulles de discussion de la messagerie 
 */
class ChatRoomScreenBis extends React.Component {
    constructor(props){
      super(props);
      this.socket = io ('http://localhost:3000', {
       transports: ['websocket'] // you need to explicitly tell it to use websockets
      });

      this.state={
        login:this.props.route.params.login,
        message:"",
        messages:[],
        recMessages:[],
        pdp:this.props.route.params.pdp,
        email: this.props.route.params.email,
        room:this.props.route.params.room
      }
    }


   /**
    * envoie les messages vers le back
    * @param messages le message qui est ecrit 
    */
    onSend (messages) {
      socket.on('')
      this.socket.emit("room", messages[0].text,this.state.pdp);
      this.setState(previousState =>({
        recMessages: GiftedChat.append(previousState.recMessages,messages),
      }))
    }

   /**
    * fonction qui stylise le message 
    * @param props le message 
    */

    renderBubble = (props) =>{
      return(
      <Bubble
        {...props}
        wrapperStyle={{
          right:  {
            backgroundColor: '#DC4444'
          },
          left: {
            backgroundColor: '#FFFFFF'
          }
        }}
        textStyle={{
          right: {
            alignItems:'left',
            color: '#fff'
          },
          left: {
            textAlign:'left',
            color: '#000' 
          }
        }}
      />)
    }
    /**
     * fonction qui envoie le message
     * @param props le message
     */

    renderSend = (props) =>{
      return(
        <Send {...props}>
          <View>
            <MaterialCommunityIcons
            name="send-circle"
            size={32}
            color= '#DC4444'
            style={{marginBottom:5, marginRight:5}}
            />
          </View>
        </Send>
      )
    }
    /**
     * Decale les messages de haut en bas, du plus anciens au plus recent 
     * @param props le message
     */
    scrollToBottomComponent = (props) =>{
      return(
        <FontAwesome
        name="angle-double-down"
        size={22}
        color= '#333'
        />
      )
    }
    
    /**
     * Une fois le component chargé, recois le message du second utilisateur du back et le met dans "recMessages"
     */
    componentDidMount(){
      this.socket.on('message', message => {
        this.setState(previousState =>({
          recMessages: GiftedChat.append(previousState.recMessages,message),
        }))
        
      });
    }

    render(){
      console.log(this.state)
        return (
            <GiftedChat
              messages={this.state.recMessages}
              onSend={messages => this.onSend(messages)}
              user={{
                _id: 1,
              }}
              renderBubble={this.renderBubble}
              alwaysShowSend
              renderSend={this.renderSend}
              scrollToBottom
              scrollToBottomComponent={this.scrollToBottomComponent}
              placeholder='Démarrer un nouveau message'
            />
          )
    }

}

export default ChatRoomScreenBis