import * as React from 'react';
import {FlatList, StyleSheet,TouchableOpacity,Image,SafeAreaView,Text} from 'react-native';
import { View } from '../components/Themed';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ChatRoomScreenBis from "./ChatRoomScreenBis";
import axios from 'axios'
import styles1 from '../Components/ChatListItems/Styles';

/**
 * component de la messagerie 
 */

class ChatScreen extends React.Component {
  constructor(props){
    super(props);
    this.state={
      login:this.props.route.params.login,
      check:false,
      like:true,
      data:{},
      loaded : false,
      room:""
    }
  }

  componentDidMount(){
     this.nameFirstname();
   }
/**
 * Récupérer les noms des l'utilisateurs avec qui on a matché du back 
 */
  nameFirstname=()=>{
    axios({
       method:'GET',
       url:'http://localhost:3000/nameFirstname/'+this.state.login,
    }).then(response=>{
      this.setState({
        loaded : true,
        data:response.data
      })
    })
  }
  /**
   * Séparer les messages en fonction du match ( Like ou Check)
   */
  choseTheDataToShow=()=>{
    if(this.state.like){
      return this.state.data.like
    }
    else if(this.state.check){
      return this.state.data.check
    }
  }
  /**
   * envoie l'email de l'utilisateur au back
   * @param item les informations de l'utilisateur 
   */
  onClick = (item) => {
    console.log(item.email)
      axios({
        method:'POST',
        url:'http://localhost:3000/idOfPersonWeWantToTalk/'+this.state.login,
        data:{
          email:item.email
        }
      }).then(response =>{
        this.props.navigation.navigate('ChatRoomScreenBis', {name: item.name,pdp:item.pdp,room:response.data})
      })
  }
  
  render() {
   if(this.state.loaded){
      
    return (
      <View style={styles.global}>
        <SafeAreaView style={styles.container}>
          <FlatList 
            style={{width: '100%',height:'100%',borderRadius:10}}
            data={this.choseTheDataToShow()}
            renderItem={({item})=> (
              <TouchableOpacity onPress={()=>this.onClick(item)}>
                <View style={styles1.container}>
                  <View style={styles1.lefContainer}>
                    <Image source={item.pdp.[0]} style={styles1.avatar}/>

                    <View style={styles1.midContainer}>
                      <Text style={styles1.username}>{item.name}</Text>
                        <Text
                          numberOfLines={2}
                          style={styles1.lastMessage}> Cliquez pour ouvrir la discussion.
                        </Text>
                    </View>
              </View>

                <Text style={styles1.time}>
                  12/45/87
                </Text>
              </View>
            </TouchableOpacity>
          )}
          keyExtractor={(item) => item.email}
        />
    
        <View style={styles.viewBouton}>
          <TouchableOpacity style={styles.bouton} onPress={()=>{this.setState({check:true,like:false})}}><Image style={styles.image} source={require('../Image/check.png')}></Image></TouchableOpacity>
          <TouchableOpacity style={styles.bouton} onPress={()=>{this.setState({like:true,check:false})}}><Image style={styles.image} source={require('../Image/coeur.png')}></Image></TouchableOpacity>
        </View>
      </SafeAreaView>
    </View>
    )}
    else{
      return(
        <SafeAreaView>

        </SafeAreaView>
      );
    }
  }
}
 
const Stack = createStackNavigator()
export default function MyStack({route}) {
     return (
         <Stack.Navigator
         screenOptions={{
             headerShown: true
           }}>
           <Stack.Screen name="ChatsScreen" component={ChatScreen} options={{headerShown: false}} initialParams={{login: route.params.login}}/>
           <Stack.Screen name="ChatRoomScreenBis" component={ChatRoomScreenBis} options={({route}) =>({title: route.params.name})}/>
         </Stack.Navigator>
       );
 }

const styles = StyleSheet.create({
  global:{
    flex: 1,
    backgroundColor: '#DC4444',
    padding: 15,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height:'100%',
    backgroundColor :"white",
    borderRadius:10,
    borderWidth:1
  },
  image:{
    width:70,
    height:70,
    resizeMode:"contain",
    borderWidth:2,
    borderRadius:35,
  },
  viewBouton:{
    height:'10%',
    width:'100%',
    flexDirection:'row',
    alignItems:"center",
    backgroundColor:'#DDDDDD',
    borderBottomLeftRadius:10,
    borderBottomRightRadius:10,
    borderTopWidth:1,
    justifyContent:'space-evenly'
    },
});